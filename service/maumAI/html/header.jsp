<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-20
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<form id="logout-form" action='/logout' method="POST">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

<!-- .lyr_service -->
<div class="lyr_service">
    <div class="lyr_service_bg"></div>
    <!-- .productWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <h1>maum.ai 서비스 구조</h1>
            <div class="landing_layers">
                <div class="stn_landing_cont">
<%--                    <div class="item_set bg_builder">--%>
<%--                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_aibuilder.png" alt="AI Builder">--%>
<%--                        <span>AI Builder</span>--%>
<%--                    </div>--%>
                    <div class="item_set bg_navy">
                        <div class="item_title">
                            <div>
                                <span>Layer 5</span>
                            </div>
                            <h3>Edge 컴퓨팅</h3>
                        </div>
                        <div class="item_cont">
                            <div class="layer5">
                                <ul>
                                    <li>
                                        <p>차량 인식</p>
                                        <p>이상 행동</p>
                                    </li>
                                    <li>
                                        <p>AI Edge device</p>
                                        <div class="img">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_edge1.svg" alt="Edge device1"/>
                                            <em class="fas fa-plus"></em>
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_edge2.svg" alt="Edge device2"/>
                                        </div>
                                    </li>
                                    <li>
                                        <p>Edge analysis</p>
                                        <div class="img">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_edge3.svg" alt="Edge analysis"/>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="item_set bg_purple">
                        <div class="item_title">
                            <div>
                                <span>Layer 4</span>
                            </div>
                            <h3>어플리케이션</h3>
                        </div>
                        <div class="item_cont">
                            <ul class="item_lst em_enter">
                                <li>
                                    <div class="inner_box contact_link" >
                                        <dl>
                                            <dt>
                                                <em>AI Builder</em>
                                            </dt>
                                            <dd class="">
                                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_aibuilder.png" alt="백그라운드">
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box contact_link" >
                                        <dl>
                                            <dt>
                                                <em>maum 회의록 </em>
                                            </dt>
                                            <dd>
                                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_maumMinutes.png" alt="백그라운드">
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box contact_link">
                                        <dl>
                                            <dt>
                                                <em>FAST 대화형 AI</em>
                                            </dt>
                                            <dd>
                                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_fastai.png" alt="백그라운드">
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box contact_link">
                                        <dl>
                                            <dt>
                                                <em>ecoMINDs<br>
                                                    서비스</em>
                                            </dt>
                                            <dd>
                                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_ecominds.svg" alt="백그라운드">
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item_set bg_blue">
                        <div class="item_title">
                            <div>
                                <span>Layer 3</span>
                            </div>
                            <h3>
                                엔진API
                            </h3>
                        </div>
                        <div class="item_cont">
                            <ul class="item_lst">
                                <li>
                                    <div class="inner_box">
                                        <dl class="">
                                            <dt>
                                                <em>음성</em>
                                            </dt>
                                            <dd>
                                                <ul>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico-spe-1-fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>음성 생성</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_2_fold.svg" class="img_m" alt="백그라운드">
                                                        </div>
                                                        <em>음성 인식</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_3_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>음성 정제</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_5_fold.svg"  alt="백그라운드">
                                                        </div>
                                                        <em>Voice Filter</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_6_fold_.svg"  alt="백그라운드">
                                                        </div>
                                                        <em>화자 인증</em>
                                                    </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl class="">
                                            <dt>
                                                <em>시각</em>
                                            </dt>
                                            <dd class="vis_box">
                                                <ul>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_lipSyncAvatar.svg" class="img_xs" alt="백그라운드">
                                                        </div>
                                                        <em>Lip Sync Avata</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg" class="img_xs" alt="백그라운드">
                                                        </div>
                                                        <em>Face-to-Face Avatar</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_6_fold_.svg" class="img_m" alt="백그라운드">
                                                        </div>
                                                        <em>얼굴 인증</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_5_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>슈퍼 레졸루션</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_vsr.svg" class="img_xs" alt="백그라운드">
                                                        </div>
                                                        <em>비디오 슈퍼 레졸루션</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_2_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>AI 스타일링</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_1_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>텍스트 제거</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_10_fold_.svg" alt="백그라운드">
                                                        </div>
                                                        <em>이미지 자막인식</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_7_fold_.svg"  class="img_m" alt="백그라운드">
                                                        </div>
                                                        <em>인물포즈인식</em>
                                                    </li>

                                                </ul>
                                                 <ul>
                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_8_fold.svg" alt="백그라운드">
                                                         </div>
                                                         <em>얼굴추적</em>
                                                     </li>

                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_face.svg" alt="백그라운드">
                                                         </div>
                                                         <em>얼굴 마스킹</em>
                                                     </li>
                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hair.svg" alt="백그라운드">
                                                         </div>
                                                         <em>헤어 컬러 인식</em>
                                                     </li>
                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_clothes.svg" alt="백그라운드">
                                                         </div>
                                                         <em>의상 특징 인식</em>
                                                     </li>
                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_4_fold.svg" alt="백그라운드">
                                                         </div>
                                                         <em>차량 번호판인식</em>
                                                     </li>
                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_windshield.svg"  class="img_m" alt="백그라운드">
                                                         </div>
                                                         <em>차량 유리창 마스킹</em>
                                                     </li>
                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_9_fold_.svg" class="img_s" alt="백그라운드">
                                                         </div>
                                                         <em>이상행동 감지</em>
                                                     </li>

                                                     <li class="landing_ico">
                                                         <div class="img">
                                                             <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_positioning.svg" class="img_xs" alt="백그라운드">
                                                         </div>
                                                         <em>치아 교정기 포지셔닝</em>
                                                     </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl class="">
                                            <dt>
                                                <em>언어</em>
                                            </dt>
                                            <dd>
                                                <ul>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_correct.svg" class="img_s" alt="백그라운드">
                                                        </div>
                                                        <em>문장 교정</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_convers.svg" class="img_s" alt="백그라운드">
                                                        </div>
                                                        <em>한글 변환</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_1_fold.svg" class="img_s" alt="백그라운드">
                                                        </div>
                                                        <em>자연어 이해</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_2_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>AI 독해</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_3_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>텍스트 분류</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_5_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>문장 생성</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_6_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>패턴분류</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_itf.svg" alt="백그라운드">
                                                        </div>
                                                        <em>의도 분류</em>
                                                    </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl class="">
                                            <dt>
                                                <em>대화</em>
                                            </dt>
                                            <dd>
                                                <ul>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_1_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>NQA 봇</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_2_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>위키봇&#47;뉴스봇</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_3_fold.svg" alt="백그라운드">
                                                        </div>
                                                        <em>호텔컨시어지봇</em>
                                                    </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li class="last_box">
                                    <div class="inner_box">
                                        <dl class="short_box">
                                            <dt>
                                                <em>분석</em>
                                            </dt>
                                            <dd>
                                                <ul>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_data_analysis.svg" alt="백그라운드">
                                                        </div>
                                                        <em>데이터 상관 분석</em>
                                                    </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="long_box">
                                            <dt>
                                                <em>영어교육</em>
                                            </dt>
                                            <dd>
                                                <ul>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_1.svg" alt="백그라운드">
                                                        </div>
                                                        <em>교육용 STT</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_2.svg" alt="백그라운드">
                                                        </div>
                                                        <em>문장 발음 평가</em>
                                                    </li>
                                                    <li class="landing_ico">
                                                        <div class="img">
                                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_3.svg" alt="백그라운드">
                                                        </div>
                                                        <em>파닉스 평가</em>
                                                    </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                     </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item_set bg_bgreen">
                        <div class="item_title">
                            <div>
                                <span>Layer 2</span>
                            </div>
                            <h3>AI 모델학습</h3>
                        </div>
                        <div class="item_cont">
                            <ul class="item_lst em_enter_down">
                                <li>
                                    <div class="inner_box">
                                        <dl>
                                            <dt>
                                                <em>Auto Machine Learning</em>
                                            </dt>
                                            <dd>
                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer2.svg" alt="백그라운드">
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item_set bg_green">
                        <div class="item_title">
                            <div>
                                <span>Layer 1</span>
                            </div>
                            <h3>데이터 &<br>정제 서비스</h3>
                        </div>
                        <div class="item_cont">
                            <ul class="item_lst em_enter_down">
                                <li>
                                    <div class="inner_box">
                                        <dl>
                                            <dt>
                                                <em>음성 Data</em>
                                            </dt>
                                            <dd>
                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl>
                                            <dt>
                                                <em>시각 Data</em>
                                            </dt>
                                            <dd>
                                                <div>
                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl>
                                            <dt>
                                                <em>언어 Data</em>
                                            </dt>
                                            <dd>
                                                <div>
                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl>
                                            <dt>
                                                <em>대화  Data</em>
                                            </dt>
                                            <dd>
                                                <div>
                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner_box">
                                        <dl>
                                            <dt>
                                                <em>분석  Data</em>
                                            </dt>
                                            <dd>
                                                <div>
                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </li>
                            </ul>
                            <div class="go_editTool">
                                <p class="contact_link">Cloud Data Edit Tool</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .stn_landing_cont 추가 -->
                <!-- .mobile 추가 -->
                <div class="stn_landing_m mobile">
                    <div class="stn_landing_cont">
<%--                        <div class="item_set bg_builder">--%>
<%--&lt;%&ndash;                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_aibuilder.png" alt="AI Builder">&ndash;%&gt;--%>
<%--                            <span>AI Builder</span>--%>
<%--                        </div>--%>
                        <div class="item_set bg_navy">
                            <div class="item_title">
                                <h3>Layer 5 Edge 컴퓨팅</h3>
                            </div>
                            <div class="swiper-container">
                                <ul class="item_lst em_enter ">
                                    <li class=" inner_box">
                                        <div class="layer5">
                                            <ul>
                                                <li>
                                                    <p>차량 인식</p>
                                                    <p>이상 행동</p>
                                                </li>
                                                <li>
                                                    <p>AI Edge device</p>
                                                    <div class="img">
                                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_edge1.svg" alt="Edge device1"/>
                                                        <em class="fas fa-plus"></em>
                                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_edge2.svg" alt="Edge device2"/>
                                                    </div>
                                                </li>
                                                <li>
                                                    <p>Edge analysis</p>
                                                    <div class="img">
                                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_edge3.svg" alt="Edge analysis"/>
                                                    </div>
                                                </li>
                                             </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item_set bg_purple">
                            <div class="item_title">
                                <h3>Layer 4 어플리케이션</h3>
                            </div>
                            <div class="swiper-container preview">
                                <ul class="item_lst em_enter swiper-wrapper">
                                    <li class="swiper-slide inner_box">
                                        <div class="inner_box" >
                                            <dl>
                                                <dt>
                                                    <em>AI Builder</em>
                                                </dt>
                                                <dd class="">
<%--                                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_fastai.png" alt="백그라운드">--%>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide inner_box">
                                        <div class="inner_box" >
                                            <dl>
                                                <dt>
                                                    <em>maum 회의록 </em>
                                                </dt>
                                                <dd>
<%--                                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_maumMinutes.png" alt="백그라운드">--%>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide inner_box">
                                        <div class="inner_box ">
                                            <dl>
                                                <dt>
                                                    <em>FAST 대화형 AI</em>
                                                </dt>
                                                <dd>
<%--                                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_voiceAlbum.png" alt="백그라운드">--%>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide inner_box">
                                        <div class="inner_box ">
                                            <dl>
                                                <dt>
                                                    <em>ecoMINDs<br>
                                                        서비스</em>
                                                </dt>
                                                <dd>
                                                    <%--                                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_landing_voiceAlbum.png" alt="백그라운드">--%>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
<%--                                <div class="link_mvp_maker mobile_move">AI Builder</div>--%>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                        <div class="item_set bg_blue">
                            <div class="item_title">
                                <h3>
                                    Layer 3 엔진API
                                </h3>
                            </div>
                            <div class="swiper-container preview2">
                                <ul class="item_lst swiper-wrapper">
                                    <li class="swiper-slide">
                                        <a class="inner_box mobile_move" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>음성</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico-spe-1-fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>음성 생성</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_2_fold.svg" class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>음성 인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_3_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>음성 정제</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_5_fold.svg"  alt="백그라운드">
                                                            </div>
                                                            <em>Voice Filter</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_6_fold_.svg"  alt="백그라운드">
                                                            </div>
                                                            <em>화자 인증</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide vis_box">
                                        <a class="inner_box mobile_move" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>시각</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_lipSyncAvatar.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>Lip Sync Avata</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>Face-to-Face Avatar</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_6_fold_.svg" class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>얼굴 인증</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_5_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>슈퍼 레졸루션</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_vsr.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>비디오 슈퍼 레졸루션</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_2_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>AI 스타일링</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_1_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>텍스트 제거</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_10_fold_.svg" alt="백그라운드">
                                                            </div>
                                                            <em>이미지 자막인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_7_fold_.svg"  class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>인물포즈인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_8_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>얼굴추적</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_face.svg" alt="백그라운드">
                                                            </div>
                                                            <em>얼굴 마스킹</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hair.svg" alt="백그라운드">
                                                            </div>
                                                            <em>헤어 컬러 인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_clothes.svg" alt="백그라운드">
                                                            </div>
                                                            <em>의상 특징 인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_4_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>차량 번호판인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_windshield.svg"  class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>차량 유리창 마스킹</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_9_fold_.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>이상행동 감지</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_positioning.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>치아 교정기 포지셔닝</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box mobile_move" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>언어</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_correct.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>문장 교정</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_convers.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>한글 변환</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_1_fold.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>자연어 이해</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_2_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>AI 독해</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_3_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>텍스트 분류</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_5_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>문장 생성</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_6_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>패턴분류</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_itf.svg" alt="백그라운드">
                                                            </div>
                                                            <em>의도 분류</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box mobile_move" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>대화</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_1_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>NQA 봇</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_2_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>위키봇&#47;뉴스봇</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_3_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>호텔컨시어지봇</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box mobile_move" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>분석</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_data_analysis.svg" alt="백그라운드">
                                                            </div>
                                                            <em>데이터<br>상관 분석</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box mobile_move" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>영어교육</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_1.svg" alt="백그라운드">
                                                            </div>
                                                            <em>교육용 STT</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_2.svg" alt="백그라운드">
                                                            </div>
                                                            <em>문장 발음 평가</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_3.svg" alt="백그라운드">
                                                            </div>
                                                            <em>파닉스 평가</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                </ul>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                        <div class="item_set bg_bgreen">
                            <div class="item_title">
                                <h3>Layer 2 AI 모델학습</h3>
                            </div>
                            <div class="">
                                <ul class="item_lst em_enter_down">
                                    <li class="">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Auto Machine Learning</em>
                                                </dt>
                                                <dd>
                                                </dd>
                                            </dl>
                                        </div>
                                     </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item_set bg_green">
                            <div class="item_title">
                                <h3>Layer 1 데이터 & 정제 서비스</h3>
                            </div>
                            <div class="swiper-container preview3">
                                <ul class="item_lst em_enter_down swiper-wrapper">
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>음성 Data</em>
                                                </dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>시각 Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>언어 Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>대화  Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>분석  Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>영어교육  Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_layer1.svg" alt="백그라운드">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <div class="go_editTool">
                                    <a href="#datatool" class="mobile_move">Cloud Data Edit Tool</a>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //.stn_landing mobile -->
            </div>


        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.productWrap -->
</div>
<!-- //.lyr_service -->

<!-- .lyr_consultant -->
<div class="lyr_consultant">
    <div class="lyr_consultant_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <div class="lyr_hd">
            <h1><span>마음 AI 컨설턴트</span>가<br>
                되기 위한 <span>4단계!</span></h1>
            <em class="fas fa-times btn_lyrWrap_close"></em>
        </div>

        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <ul>
                <li>
                    <p>
                        <strong>1. Join us</strong>
                        <span>마음AI 컨설턴트 자세히 알아보고 지원하기!</span>
                        <a href="https://www.notion.so/Challenge-Now-AI-Consultant-bda57094ed33449facb23df29d217e5d" target="_blank">안내서 보러가기 <em class="fas fa-angle-right"></em></a>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>2. Study & Learn</strong>
                        <span>maum.ai에 접속하여 교육 모델을 체험하고, </span>
                        <span>아카데미 강좌를 통해 실력을 키우기!</span>
                        <a href="http://maumacademy.maum.ai/" target="_blank" class="academy_btn">아카데미 바로가기 <em class="fas fa-angle-right"></em></a>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>3. Take a Test</strong>
                        <span>학습 내용을 활용하여 최종 시험을 진행하기!</span>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>4. Get Started!</strong>
                        <span>자격증을 받고, 바로 고객 대상 AI 컨설팅을 시작!</span>
                    </p>
                </li>
            </ul>


        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_consultant -->

<!-- .lyr_login -->
<div class="lyr_login">
    <div class="lyr_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <div class="lyr_hd">
            <h1>로그인</h1>
            <em class="fas fa-times btn_lyr_close"></em>
        </div>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <form class="loginWrap" method="get" action="">
                <fieldset>
                    <legend>Login</legend>
                    <div class="loginBox">
                        <span>
                            <input type="text" name="ipt_id" id="ipt_id" class="ipt_txt" title="User Name" placeholder="이메일" >
                        </span>
                        <span>
                            <input type="password" name="ipt_pw" id="ipt_pw" class="ipt_txt" title="Password" placeholder="비밀번호" >
                        </span>
                        <span class="checks">
                            <input type="checkbox" name="ipt_check" id="ipt_check" class="ipt_check">
                            <label for="ipt_check"><em class="far fa-check-circle"></em> 이메일 저장</label>
                            <!-- [D] 에러일 경우 .txt_error 스크립트로 구현 -->
                            <span class="txt_error">가입하지 않은 이메일이거나, 잘못된 비밀번호 입니다.</span>
                        </span>
                        <span>
                            <a href="#none" title="로그인" class="loginbtn"> 로그인</a>
                        </span>
                    </div>
                    <span class="or">또는</span>
                </fieldset>
            </form>
            <div class="btn_box">
                <a href="javascript:login();" title="구글로그인" class="google_login"> 구글 로그인</a>
                <a href="javascript:login_facebook();" title="페이스북로그인" class="facebook_login">페이스북 로그인</a>
            </div>
            <div class="btn">
                <a href="/home/agree" class="btn_signup">회원가입 </a>
                <span>&nbsp;|&nbsp;</span>
                <a href="#none"  class="btn_forgot">비밀번호 찾기</a>

            </div>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_login -->
<!-- .lyr_password -->
<div class="lyr_password">
    <div class="lyr_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <div class="passwordWrap">
                <em class="fas fa-times btn_lyr_close"></em>
                <div class="passwordBox">
                    <h5>비밀번호 찾기</h5>
                    <p>가입한 이메일 주소를 입력해 주세요.</p>
                    <div class="email_type">
                        <label for="email"></label>
                        <input type="email" id="email" value="">
                        <a class="email_send" href="#none" title="결제정보 등록하기">보내기</a>
                    </div>
                    <div class="code_box">
                        <label for="code" style="display: none"></label>
                        <input type="number" name="code" id="code" class="" value="" placeholder="인증 코드 입력">
                        <em class="count">02:30</em>
                        <button class="email_code" type="button" id="email_code">확인</button>
                        <span class="email" id="code_noti">인증이 성공했습니다.</span>
                    </div>
                    <div class="login_btn">
                        <button class="login_pop" type="button">로그인</button>
                    </div>
                </div>
                <div class="passwordChangeBox">
                    <h5>비밀번호 변경</h5>
                    <label for="password">새 비밀번호</label>
                    <input type="password" id="password" value="">
                    <span class="noti">*8~12자리 영문,숫자 포함</span>
                    <label for="passwordCheck">새 비밀번호 재확인</label>
                    <input type="password" id="passwordCheck" value="">
                    <span class="error password">비밀번호가 일치하지 않습니다.</span>
                    <a class="passwordChange" href="#none" title="비밀번호 변경">비밀번호 변경</a>
                </div>

            </div>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_password -->

    <!-- AMR 로그인이 되면 .aside에 .logged_in class가 추가되어야 합니다. -->
    <div class="aside" style="display:none;">
        <!-- .aside_top -->
        <div class="aside_top">
<%--            <h1><a href="/"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maumAI logo"></a></h1>--%>

            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <a class="btn_sign login_pop_m" href="#none">로그인</a>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <!-- AMR 아이콘이 생기고 user계정은 span 태그로 감싸집니다 -->
            <sec:authorize access="isAuthenticated()">
                <a class="btn_sign" href="#none"><i class="fas fa-user-circle"></i><span>${fn:escapeXml(sessionScope.accessUser.email)}</span></a>
            </sec:authorize>

        </div>
        <!-- //.aside_top -->
        <!-- .aside_mid -->
        <div class="aside_mid">

            <ul class="m_nav">
<%--                <li>--%>
<%--                    <h2><a href="#none">HOME</a></h2>--%>
<%--                </li>--%>

                <!-- 로그인 O인 경우 -->
                <sec:authorize access="isAuthenticated()">
                    <li>
                        <!-- AMR m_nav h2에 아이콘이 추가됩니다 -->
                        <h2><a href="#none" class=""><i class="fas fa-user"></i>마이페이지 <em class="fas fa-chevron-down"></em></a></h2>
                        <ul class="m_lst">
                            <li>
                                <h3><a href="${pageContext.request.contextPath}/user/profileMain" target="_blank" title="프로필" class="layer_m_btn">프로필</a></h3>
                                <h3><a href="${pageContext.request.contextPath}/user/apiAccountMain" target="_blank" title="API 정보" class="layer_m_btn">API 정보</a></h3>
                                <h3><a href="${pageContext.request.contextPath}/user/paymentInfoMain" target="_blank" title="결제정보" class="layer_m_btn">결제정보</a></h3>
                              <!-- AMR 로그아웃 .aside_btm에 포함되어 리스트에서 없어집니다 -->
                        </ul>
                    </li>
                </sec:authorize>

                <li>
                    <!-- AMR m_nav h2에 아이콘이 추가됩니다 -->
                    <h2><a href="#none" class=""><i class="fas fa-clone"></i>서비스 바로가기 <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
<%--                        <li>--%>
<%--                            <h3><a href="#none" title="maum.ai 서비스 구조" class="layer_m_btn">maum.ai 서비스 구조</a></h3>--%>
<%--                        </li>--%>
                        <li>
                            <h3><a href="${pageContext.request.contextPath}/main/krMainHome">Cloud API</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://builder.maum.ai/landing">AI Builder</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://minutes.maum.ai">maum 회의록</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://fast-aicc.maum.ai">FAST 대화형 AI</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://data.maum.ai/?lang=ko">maum DATA</a></h3>
                        </li>
                    </ul>
                </li>
                <li>
                    <!-- AMR m_nav h2에 아이콘이 추가됩니다 -->
                    <h2><a href="http://bit.ly/mindslab_recruit" target="_blank"><i class="fas fa-street-view"></i>채용공고</a></h2></li>
                <li>
                    <!-- AMR m_nav h2에 아이콘이 추가됩니다 -->
                    <h2><a href="${pageContext.request.contextPath}/home/pricingPage?lang=ko"><i class="fas fa-credit-card"></i>가격정책</a></h2>

                </li>
                <li>
                <!-- AMR 마음아카데미 >> 아카데미로 수정됩니다 / m_nav h2에 아이콘이 추가됩니다 -->
                    <h2><a href="http://maumacademy.maum.ai/"><i class="fas fa-graduation-cap"></i>아카데미</a></h2>
                </li>
                <!-- AMR 에코마인즈 리스트가 삭제됩니다 -->
                <li>
<%--                    <h2><a href="javascript:openConsultantPop()" class="go_const">AI 컨설턴트 신청</a> </h2>--%>
                </li>
                <!-- AMR 로그아웃 리스트가 .aside_btm에 포함되어 리스트에서 없어집니다 -->
            </ul>
        </div>
        <!-- //.aside_mid -->

        <!-- .aside_btm -->
        <div class="aside_btm">
            <ul>
                <li class=""><span>한국어</span></li>
                <li class=""><span><a href="/?lang=en">English</a></span></li>
            </ul>
            <!-- AMR 로그아웃 리스트 추가 / 로그인이 되면 로그아웃 리스트가 보여집니다. -->
            <div class="maum_logout">
                <a href="#none" onclick="document.getElementById('logout-form').submit();" title="로그아웃"><i class="fas fa-sign-out-alt"></i> <span>로그아웃</span></a>
            </div>
        </div>
        <!-- //.aside_btm -->
    </div>

<%--    <div class="bg_aside"></div>--%>
<%--    <a class="btn_header_ham" href="#none"><span class="hamburger_icon"></span></a>--%>
    <!-- //aside -->


<%--    <div class="common_header">--%>
<%--        <div class="header_box">--%>
<%--            <h1><a href="/?lang=ko"><img class="header_logo" src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo"></a></h1>--%>
<%--            <!--.sta-->--%>
<%--            <div class="sta">--%>
<%--                <a href="/home/krEmployeesMain" class="go_employees" id="Button_Internal" hidden>직원용</a>--%>
<%--                <a href="#" onClick="go(); return false;" class="go_service">서비스 </a>--%>
<%--&lt;%&ndash;                javascript:scrollTo(0,623)&ndash;%&gt;--%>
<%--                <a href="/home/pricingPage?lang=ko" class="go_price">가격정책 </a>--%>
<%--                <a href="/home/academyForm" class="go_academy">마음아카데미</a>--%>
<%--                <a href="/home/krEcomindsMain" class="go_ecominds">ecoMINDs</a>--%>
<%--                <a href="javascript:openConsultantPop()" class="go_const">AI 컨설턴트 모집</a>--%>
<%--&lt;%&ndash;                <div id="menu">&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <p class="service_btn">서비스 바로가기 &nbsp;&nbsp;&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <em class="fas fa-angle-down"></em>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <em class="fas fa-angle-up"></em>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </p>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <ul class="dropdown-menu">&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <li><a href="#none" title="maum.ai 서비스 구조" class="layer_btn">maum.ai 서비스 구조</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <li><a href="https://builder.maum.ai/landing" title="AI Builder" target="_blank">AI Builder</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <li><a href="/main/krMainHome" title="클라우드 API" target="_blank">Cloud API</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <li><a href="https://minutes.maum.ai/"  title="회의록 바로가기" target="_blank">maum 회의록</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <li><a href="https://fast-aicc.maum.ai" target="_blank" title="대화형 AI">FAST 대화형 AI</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                <li><a href="https://data.maum.ai/?lang=ko" target="_blank" title="데이터 서비스">maum DATA</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    </ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                </div>&ndash;%&gt;--%>

<%--                <!-- 로그인 X인 경우 -->--%>
<%--                <sec:authorize access="isAnonymous()">--%>
<%--                    <a class="btn_sign" href="javascript:login();">로그인</a>--%>
<%--                </sec:authorize>--%>

<%--                <!-- 로그인 O인 경우 -->--%>
<%--                <sec:authorize access="isAuthenticated()">--%>
<%--                    <div class="user_info">--%>
<%--                        <div class="userBox">--%>
<%--                            <ul>--%>
<%--                                <li>--%>
<%--                                    <p class="ico_user">--%>
<%--                                        <em class="far fa-user"></em>--%>
<%--                                        <span>${fn:escapeXml(sessionScope.accessUser.email)}</span>--%>
<%--                                        <em class="fas fa-angle-down"></em>--%>
<%--                                        <em class="fas fa-angle-up"></em>--%>
<%--                                    </p>--%>
<%--                                    <ul class="lst">--%>
<%--                                        <li class="ico_profile"><a target="_self" href="/user/profileMain">프로필</a></li>--%>
<%--                                        <li class="ico_account"><a target="_self" href="/user/apiAccountMain">API 정보</a></li>--%>
<%--                                        <li class="ico_payment"><a target="_self" href="/user/paymentInfoMain">결제정보</a></li>--%>
<%--                                        <li class="ico_logout"><a href="#" onclick="document.getElementById('logout-form').submit();">로그아웃</a></li>--%>
<%--                                    </ul>--%>
<%--                                </li>--%>
<%--                            </ul>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </sec:authorize>--%>

<%--                <div class="lang_box">--%>
<%--                    <span>한국어</span>--%>
<%--                    <span><a href="/?lang=en" target="_self">English</a></span>--%>
<%--                </div>--%>
<%--                <!--.etcMenu-->--%>
<%--                &lt;%&ndash;					<div class="etcMenu">&ndash;%&gt;--%>
<%--                &lt;%&ndash;						<ul>&ndash;%&gt;--%>
<%--                &lt;%&ndash;							<li class="lang">&ndash;%&gt;--%>
<%--                &lt;%&ndash;								<p class="lang_select">한국어 <em class="fas fa-chevron-down"></em></p>&ndash;%&gt;--%>
<%--                &lt;%&ndash;								<ul class="lst">&ndash;%&gt;--%>
<%--                &lt;%&ndash;									<li><a href="/login/loginForm?lang=en" target="_self"><em>English</em></a></li>&ndash;%&gt;--%>
<%--                &lt;%&ndash;								</ul>&ndash;%&gt;--%>
<%--                &lt;%&ndash;							</li>&ndash;%&gt;--%>
<%--                &lt;%&ndash;						</ul>&ndash;%&gt;--%>
<%--                &lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--                <!--//.etcMenu-->--%>
<%--            </div>--%>
<%--            <!--//.sta-->--%>
<%--        </div>--%>

<%--    </div>--%>


<!-- #header -->
<div id="header">
    <!-- .maum_sta -->
    <div class="maum_sta">
        <h1><a href="/">maum.ai</a></h1>
        <!-- .maum_gnb -->
        <div class="maum_gnb">
            <ul class="nav">
                <sec:authorize access="hasAnyRole('ADMIN', 'INTERNAL')">
                    <li><a href="${pageContext.request.contextPath}/home/krEmployeesMain" class="go_employees" id="Button_Internal">직원용</a></li>
                </sec:authorize>
                <li><a href="#none" onClick="go(); return false;" class="">서비스</a></li>
                <!-- AMR 마음아카데미 >> 아카데미로 수정됩니다 -->
                <li><a href="http://maumacademy.maum.ai/" class="">아카데미</a></li>
                <li><a href="${pageContext.request.contextPath}/home/pricingPage?lang=ko" class="">가격정책</a></li>
                <li><a href="http://bit.ly/mindslab_recruit" target="_blank">채용공고</a></li>
                <li><a href="/event/maumBook" target="_blank">이벤트</a></li>
<%--                <li><a href="https://edge.maum.ai/" class="">maum Edge</a></li>--%>
<%--                <li><a href="javascript:openConsultantPop()">AI 컨설턴트 모집</a></li>--%>
<%--                <li><a href="${pageContext.request.contextPath}/home/krEcomindsMain" class="">ecoMINDs</a></li>--%>
            </ul>
        </div>
        <!-- //.maum_gnb -->
        <!-- .maum_etc -->
        <div class="maum_etc">
            <ul class="nav">
              
                <!-- 로그인X -->
                <sec:authorize access="isAnonymous()">
                <li><a class="btn_sign login_pop" href="#none">로그인</a></li>
                </sec:authorize>
                <!-- 로그인O-->
                <sec:authorize access="isAuthenticated()">
                <!-- AMR 아이콘이 생기고 '사용자' text는 span 태그로 감싸집니다 -->
                <li><a class="btn_ico user" href="#none"><i class="fas fa-user-circle"></i><span>사용자</span></a>
                    <div class="lstBox">
                        <ul class="lst">
                            <li class="userInfo">
                                <span class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/ico_user_g.svg" alt="사용자"></span>
                                <span class="txt">
                                    <em class="userName">${fn:escapeXml(sessionScope.accessUser.name)}</em>
                                    <em class="loginID">${fn:escapeXml(sessionScope.accessUser.email)}</em>
                                </span>
                            </li>
                            <li><a href="${pageContext.request.contextPath}/user/profileMain" target="_self" title="프로필">프로필</a></li>
                            <li><a href="${pageContext.request.contextPath}/user/apiAccountMain" target="_self" title="API 정보">API 정보</a></li>
                            <li><a href="${pageContext.request.contextPath}/user/paymentInfoMain" target="_self" title="결제정보">결제정보</a></li>
                            <li><a href="#none" onclick="document.getElementById('logout-form').submit();" title="로그아웃">로그아웃</a></li>
                        </ul>
                    </div>
                </li>
                </sec:authorize>

                <!-- 브라우저의 IP 조회 -->
<%--                <script type="application/javascript">
                    var is_internal = false;

                    function getIP(json) {
                        // console.log("ip -------------------------- > ", json.ip);
                        if(json.ip == '125.132.250.204') {
                            $('#Button_Internal').show();
                            is_internal = true;
                        }
                        else $('#Button_Internal').hide();
                    }
                </script>--%>

                <li><a class="btn_ico lang" href="#none"><i class="fas fa-globe"></i><span>언어</span></a>
                    <div class="lstBox">
                        <ul class="lst">
                            <li class="lang_li"><a href="#none">한국어</a></li>
                            <li class="lang_li"><a href="/?lang=en" target="_self" title="English">English</a></li>
                        </ul>
                    </div>
                </li>
                <li><a class="btn_ico app" href="#none">웹 앱 및 서비스</a>
                    <div class="appBox">
                        <div class="tit">웹 앱 및 서비스</div>
                        <ul class="lst">
                            <li>
                                <a href="${pageContext.request.contextPath}/main/krMainHome" target="_blank" title="Cloud API">
                                    <span class="ico"></span>
                                    <em>Cloud API</em>
                                </a>
                            </li>
                            <li>
                                <a href="https://builder.maum.ai/landing?lang=ko" target="_blank" title="AI Builder">
                                    <span class="ico"></span>
                                    <em>AI Builder</em>
                                </a>
                            </li>

                            <li>
                                <a href="https://minutes.maum.ai/" target="_blank" title="maum 회의록">
                                    <span class="ico"></span>
                                    <em>maum 회의록</em>
                                </a>
                            </li>
                            <li>
                                <a href="https://fast-aicc.maum.ai/login?lang=ko" target="_blank" title="FAST 대화형 AI">
                                    <span class="ico"></span>
                                    <em>FAST 대화형 AI</em>
                                </a>
                            </li>
                            <li>
                                <a href="https://ava.maum.ai/login" target="_blank">
                                    <span class="ico"></span>
                                    <em>AVA</em>
                                </a>
                            </li>
                            <li>
                                <a href="https://edge.maum.ai/" target="_blank">
                                    <span class="ico"></span>
                                    <em>Edge AI Platform</em>
                                </a>
                            </li>
                            <li>
                                <a href="https://data.maum.ai/?lang=ko" target="_blank" title="maum DATA">
                                    <span class="ico"></span>
                                    <em>maum DATA</em>
                                </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/home/krEcomindsMain" target="_self" title="eco MINDs">
                                    <span class="ico"></span>
                                    <em>eco MINDs</em>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <!-- //.maum_etc -->
        <a class="btn_maum_ham" href="#none">모바일 전체메뉴
            <span class="line_top"></span>
            <span class="line_mid"></span>
            <span class="line_btm"></span>
        </a>
    </div>
    <!-- //.maum_sta -->
    <!-- .svc_sta -->
    <div class="svc_sta">
        <!-- svc_visual -->
        <div class="svc_visual">
            <h3>Edge AI Platform</h3>
            <p class="bg_img"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_visual01.png" alt="Edge AI 이미지"></p>
        </div>
        <!-- //svc_visual -->
        <!-- .lnb -->
        <div class="lnb">
            <h2><a href="https://edge.maum.ai/" title="maum Edge" alt="Edge AI Platform">Edge AI Platform</a></h2>

            <!-- [D] 로컬메뉴  -->
            <ul class="nav">
                <li><a class="active" href="#stn_overview">Overview</a></li>
                <li><a href="#stn_edgeDevice">Edge AI Device</a></li>
                <li><a href="#stn_edgeCloud">AI Computing Cloud</a></li>
                <li><a href="#stn_svcCase">고객 사례</a></li>
                <li><a href="#inquiry">문의하기</a></li>
            </ul>
        </div>
        <!-- //.lnb -->
    </div>
    <!-- //.svc_sta -->
</div>
<!-- //#header -->



<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper.min.js"></script>

<script type="text/javascript">

    function login(){
        var path= window.location.pathname;
        location.href = "${google_url}" +"?targetUrl="+path;
    }

    function openConsultantPop(){
        $('.lyr_consultant').fadeIn();
        $('.btn_header_ham').removeClass('active');
        $('.aside').css('width','0');
    }
    function go(){
        var url = window.location.href;
        var pram = location.search;
        var path = location.pathname;
        console.log(pram);
        // var service = document.getElementsByClassName('go_service');
        if(path == '/'){
            scroll();
        }else {
            // scroll();
            window.location.href="/?lang=ko#service_position";
            // $('.lyr_service').fadeIn();
        }
    }
    function scroll(){
        scrollTo(0,660);
    }

    $(document).ready(function() {
        $('.svc_sta').css('display','none');
        var pathname = location.pathname;
        // if(pathname === "/home/maumEdge"){
        //     $('.svc_sta').css('display','block');
        // }

        let $ClientId = "${client_id}";
        let $RedirectUri = "${redirect_uri}";
        let $SsoUrl = "${sso_url}";

        applySwiper('.preview', { slidesPerView: 1.2});
        applySwiper('.preview2', { slidesPerView: 1.6 });
        applySwiper('.preview3', { slidesPerView: 3 });
        applySwiper('.success_logo', {
            slidesPerView: 5.8,
            spaceBetween: 28,
            slidesPerGroup : 6,
            loop : true,
            loopFillGroupWithBlank : true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });
        applySwiper('.success_logo_m', {
            slidesPerView: 3.1,
            spaceBetween: 20,
            slidesPerGroup : 3,
            loopFillGroupWithBlank : true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });

        function applySwiper(selector, option) {
            var defaultOption = {
                speed : 200,
                slidesPerView: 1,
                spaceBetween: 22,
                centeredSlides: false,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                }
            };
            return new Swiper(selector, $.extend(defaultOption, option))
        }


        //서비스 구조 보기 웹
        $('.layer_btn').on('click', function () {
            $('.lyr_service').fadeIn();
            $('body').css({
                'overflow': 'hidden'
            });

        });
        $(".dropdown-menu li a").on('click', function () {
            $('.dropdown-menu').hide().parent().parent().removeClass('active');
            $(".lst").hide().parent().parent().removeClass('active');
        });

        //서비스 구조 보기 모바일
        $('.layer_m_btn').on('click', function () {

            $('.lyr_service').fadeIn().css('visibility', 'visible');
            $('.btn_header_ham').removeClass('active');
            $('.aside').animate({
                width: '0'
            }, {duration: 200, queue: false});

            $('.bg_aside').animate({
                opacity: 0,
                display: 'none',
            }, {duration: 150, queue: false});
            // $('.bg_aside').css({
            // 	display : 'none',
            // });
            $('body').css({
                'overflow': 'hidden'
            });

        });

        // header user
        $('.userBox dl dd > a').on('click',function(){
            $(this).parent().parent().addClass('active');
        });
        $('.contents').on('click',function(){
            $('.userBox dl').removeClass('active');
        });

        //aside menu
        var clicked = false;
        var asideWidth = '100%'
        $('.aside').show();

        $('a.btn_maum_ham').click(function(){
            console.log(clicked);
            if (!clicked) {
                $(this).addClass('active');
                $('.aside').animate({
                    width : asideWidth
                },{duration:200,queue:false});
                $('.btn_goTop').hide();

                $('.bg_aside').animate({
                    opacity : 0.7,
                    display : 'block'
                },{duration:200,queue:false});

                $('body').css({
                    overflow : 'hidden'
                });

                clicked=true;
            } else {
                $(this).removeClass('active');
                $('.aside').animate({
                    width: '0'
                },{duration:200,queue:false});
                $('.btn_goTop').show();

                $('.bg_aside').animate({
                    opacity : 0,
                    display : 'none'
                },{duration:150,queue:false});

                $('body').css({
                    overflow : ''
                });

                clicked=false;
            }
        });

        $('.bg_aside').on('click',function(){
            $('a.btn_maum_ham').removeClass('active');
            $('.aside').animate({
                width: '0'
            },{duration:200,queue:false});
            $('.btn_goTop').show();

            $('.bg_aside').animate({
                opacity : 0,
                display : 'none'
            },{duration:150,queue:false});

            $('body').css({
                overflow : ''
            });
            clicked=false;
        });


        $('.btn_lyrWrap_close, .lyr_consultant_bg, .lyr_service_bg').on('click', function () {
            $('.lyr_info').hide();
            $('.lyr_service').fadeOut();
            $('.lyr_consultant').hide();
            $('#info_name').val("");
            $('#info_company').val("");
            $('#info_email').val("");
            $('#info_phone').val("");
            $('body').css({
                'overflow': ''
            });
            clicked = true;
        });
        // $(".service_btn").on('click', function () {
        //     $(this).parent().parent().toggleClass('active');
        //     $(".dropdown-menu").slideToggle(200);
        // });
        // $(".ico_user").on('click', function () {
        //     $(this).parent().parent().toggleClass('active');
        //     $(".lst").slideToggle(200);
        // });
        // $('.login_box, #wrap, #user_container').on('click', function () {
        //     $('.dropdown-menu').hide().parent().parent().removeClass('active');
        //     $(".lst").hide().parent().parent().removeClass('active');
        //
        // });

        // 20200825 - 자체 로그인 기능 추가 - MRS //
        // 20200922 - SSO 페이지로 redirect 되도록 수정 - LYJ //
        //------------------------------------------------------------------------//

        //해더 로그인 버튼
        $('.btn_sign').on('click',function(){
            var stateVal = uuidv4();

            location.href = $SsoUrl+"/maum/oauthLoginMain" + "?response_type=code&client_id=" + $ClientId + "&redirect_uri=" + encodeURIComponent($RedirectUri);
                //+ "&state=" + stateVal;

            //$('.lyr_login').fadeIn();
            //$('.lyr_login .lyrWrap').fadeIn();
            //$('.lyr_password').fadeOut();
        });

        function uuidv4() {
            return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        //모바일 로그인 버튼
        $('.login_pop_m').on('click',function(){
            $('.lyr_login').fadeIn();
            $('.lyr_login .lyrWrap').fadeIn();
            $('.lyr_password').fadeOut();
            $('.btn_header_ham ').trigger('click');

        });

        $('.lyr_bg, .btn_lyr_close').on('click',function(){
            $('.lyr_login').fadeOut();
            $('.lyr_password').fadeOut();
        })

        //자체 로그인 버튼
        $(".loginbtn").on('click',function(){
            //임시
            $('.checks label').hide();
            $('.txt_error').fadeIn();

           //에러 일때
           //var error;
           //if(error){
           //   $('.checks label').hide();
           //     $('.txt_error').fadeIn();
           // }else {
               //자체 로그인 후 홈 화면
           // }
        });

        //비밀번호 찾기
        $(".btn_forgot").on('click', function(){
            $('.lyr_login').hide();
            $('.lyr_password').fadeIn();
            $('.lyr_password .lyrWrap').fadeIn();
        })

        //비밀번호 이메일 보내기 버튼
        $('.email_send').on('click', function(){
            $('.email_type').hide();
            $(".passwordBox h5").text('비밀번호 변경 인증 코드');
            $(".passwordBox p").text('입력하신 메일 주소로 발송한 인증코드를 입력해주세요.');
            $('.code_box').fadeIn();
        })

        //비밀번호 이메일 코드 입력
        $(".email_code").on('click', function(){
            $('#code_noti').fadeIn();
            $('.passwordChangeBox').fadeIn();
            //만약 코드가 맞지 않은 경우
            //$('#code_noti').fadeIn().addClass('error').text('코드 번호를 다시 확인해주세요.');
        })

        //비밀번호 체크
        $('#password').keyup(function(){
            $(this).removeClass('error');
            $('.noti').removeClass('error');
            $('.password').hide();
        });

        //비밀번호 재확인 체크
        $('#passwordCheck').on('input keyup paste', function(){
            var password = $('#password').val();
            var password_check = $('#passwordCheck').val();
            if(password_check.length > 0){
                if (password !== password_check) {
                    $('.password').css('display','block').addClass('error');
                }else{
                    $('.password').hide();
                }
            }else {
                $('.password').hide();
            }
        });


        //최종 비밀번호 변경 버튼
        $(".passwordChange").on('click', function(){

            //비밀번호 유효성 체크
            var pw = $("#password").val();
            var pw_input = $("#password");
            var num = pw.search(/[0-9]/g);
            var eng = pw.search(/[a-z]/ig);
            if(pw.length < 8 || pw.length > 12){
                alert("비밀번호는 8자리 ~ 12자리 이내로 입력해주세요.");
                $('.noti').addClass('error');
                $('#passwordCheck').val('');
                pw_input.addClass('error');
                return false;
            }else if(pw.search(/\s/) != -1){
                alert("비밀번호는 공백 없이 입력해주세요.");
                $('.noti').addClass('error');
                $('#passwordCheck').val('');
                pw_input.addClass('error');
                return false;
            }else if(num < 0 || eng < 0  ){
                alert("영문,숫자를 혼합하여 입력해주세요.");
                $('.noti').addClass('error');
                $('#passwordCheck').val('');
                pw_input.addClass('error');
                return false;
            }else {
                $(".passwordBox h5").text('비밀번호 변경 완료');
                $(".passwordBox p").text('비빌번호 변경이 완료 되었습니다.');
                $('.code_box').hide();
                $('.passwordChangeBox').hide();
                $('.login_btn').fadeIn();
                return true;
            }
        })
    });
</script>
