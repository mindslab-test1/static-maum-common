var callbackVoiceFilter = null; // callback
var vf_inputType = "";
var vf_MixedAudio = null;
var vf_ReqAudio = null;

$(document).ready(function () {

    // 파일 업로드 (mixedAudio)
    $('#vfMixedInputFile').on('change', function () {
        onClick_VF_fileUpload($(this), "mix");
    });

    // 파일 업로드 (reqAudio)
    $('#vfReqInputFile').on('change', function () {
        onClick_VF_fileUpload($(this), "req");
    });

    // 재생 & 중지 (mixedAudio)
    $('#voiceFilter_input_box .MixVoice_play_btn').on('click', function () {
        let text = $(this).text();
        if (text === messages.common.play) {
            playVFAudio('mix');
            $(this).text(messages.common.pause);
        } else {
            pauseVFAudio('mix');
            $(this).text(messages.common.play);
        }
    });
    // 재생 & 중지 (reqAudio)
    $('#voiceFilter_input_box .ReqVoice_play_btn').on('click', function () {
        let text = $(this).text();
        if (text === messages.common.play) {
            playVFAudio('req');
            $(this).text(messages.common.pause);
        } else {
            pauseVFAudio('req');
            $(this).text(messages.common.play);
        }
    });

    // 녹음 시작  ======================================> 이거 해야함
    $('#mixAudioRecord_btn').on('click', function () {
        $('#vf_req_RecordStop').hide();
        $('#vf_mix_RecordStop').show();
        onClick_VF_record();
    });
    $('#reqAudioRecord_btn').on('click', function () {
        $('#vf_req_RecordStop').show();
        $('#vf_mix_RecordStop').hide();
        onClick_VF_record();
    });

    // 녹음 중지
    $('#vf_mix_RecordStop').on('click', onClick_VF_MixstopRecording);
    $('#vf_req_RecordStop').on('click', onClick_VF_ReqstopRecording);

    // 삭제 (녹음 파일 or 업로드한 파일)
    $('#voiceFilter_input_box .MixVoice_delete').on('click', onClick_VF_deleteMixedAudio);
    $('#voiceFilter_input_box .ReqVoice_delete').on('click', onClick_VF_deleteReqAudio);

    // 입력 완료
    $('#vf_input_complete').on('click', onClick_VF_inputComplete);

    // close
    $('#voiceFilter_input_box .ico_close').on('click', function () {
        $('#voiceFilter_input_box').fadeOut(300);

        if (is_audio_recording)
            is_audio_recording = false;

        stopAndClearRecording();
        clear_VF_inputData();

        $('body').css({
            'overflow': '',
        });

        if (callbackVoiceFilter != null)
            callbackVoiceFilter(null, null);
    });

});

/* ===================================================================================================================== */
// 사용자 호출
/* ===================================================================================================================== */
function showPopup_VoiceFilter(vf_engine, callback) {
    console.log("showPopup_VoiceFilter !!!");

    $('#voiceFilter_input_box .record_btn').attr('disables', false);
    $('#voiceFilter_input_box .record_btn').removeClass('disabled');

    callbackVoiceFilter = callback;
    vf_inputType = vf_engine.inputType;

    switch (vf_inputType) {
        case 'none':
            callbackVoiceFilter('none', 'none');
            break;
        case 'multi':
            setReadyAudioAPI(VF_audioErr_callback);
            init_VF_Popup();
            break;
        default:
            console.log("Voice Filter : inputType 오류");
            callbackVoiceFilter(null, null);
    }
}

/* ===================================================================================================================== */
// 버튼 클릭
/* ===================================================================================================================== */
function onClick_VF_inputComplete() {
    if (vf_MixedAudio === "" || vf_MixedAudio === null) {
        console.log("Mixed Audio 없음");
        return;
    } else if (vf_ReqAudio === "" || vf_ReqAudio === null) {
        console.log("Req Audio 없음");
        return;
    } else {
        stopAndClearRecording();
        let multiObj = {
            "mixedVoice": vf_MixedAudio,
            "reqVoice": vf_ReqAudio
        };
        callbackVoiceFilter('multi', multiObj);
    }

    $('#voiceFilter_input_box').fadeOut(300);
    clear_VF_inputData();
}

// '업로드' 버튼 클릭 시
function onClick_VF_fileUpload(inputObj, classifyParam) {

    console.log('File upload ==> ' + classifyParam);

    if (classifyParam == "mix") {
        vf_MixedAudio = inputObj.get(0).files[0];

        if (vf_MixedAudio === null || vf_MixedAudio === "") {
            return false;
        } else {
            let url = URL.createObjectURL(vf_MixedAudio);
            let audio = document.getElementById("AudioPlayer_mix");
            audio.src = url;
            audio.addEventListener('ended', mixedAudio_pause_evt);

            set_VF_playUI('mix');
        }
    } else if (classifyParam == "req") {
        vf_ReqAudio = inputObj.get(0).files[0];

        if (vf_ReqAudio === null || vf_ReqAudio === "") {
            return false;
        } else {
            let url = URL.createObjectURL(vf_ReqAudio);
            let audio = document.getElementById("AudioPlayer_req");
            audio.src = url;
            audio.addEventListener('ended', reqAudio_pause_evt);

            set_VF_playUI('req');
        }
    }
}

function mixedAudio_pause_evt() {
    //console.log("mix 리스너");
    $('#voiceFilter_input_box .MixVoice_play_btn').text(messages.common.play);
    //document.getElementById("AudioPlayer_mix").removeEventListener('ended', mixedAudio_pause_evt);
}

function reqAudio_pause_evt() {
    //console.log("req 리스너");
    $('#voiceFilter_input_box .ReqVoice_play_btn').text(messages.common.play);
    //document.getElementById("AudioPlayer_req").removeEventListener('ended', reqAudio_pause_evt);
}

function onClick_VF_record() {

    console.log(' @ recording start !!! ');

    is_audio_recording = true;
    startRecording();

    $('#voiceFilter_input_box .input_box').hide();
    $('#voiceFilter_input_box .btn').hide();
    $('#voiceFilter_input_box .recording').show();
    $('#voiceFilter_input_box .recording .record_Box').show();
    $('#voiceFilter_input_box .recording .record_desc').show();
}

function onClick_VF_MixstopRecording() {

    is_audio_recording = false;
    stopRecording();

    audio_recorder && audio_recorder.exportMonoWAV(function (blob) {
        //console.log(' @ stop recording ------------------------------ 3-1 ');
        vf_MixedAudio = blob;
        let url = URL.createObjectURL(blob);
        let audio = document.getElementById("AudioPlayer_mix");
        audio.src = url;

        // 파일 다운로드
        /*var link = document.createElement("a");
        link.download = "111.wav";
        link.href = url;
        link.click();*/

        audio.addEventListener('ended', mixedAudio_pause_evt);
        set_VF_playUI('mix');

    }, "audio/wav", 16000);

    set_VF_afterRecordUI();
    setReadyAudioAPI(null);
}

function onClick_VF_ReqstopRecording() {

    is_audio_recording = false;
    stopRecording();

    audio_recorder && audio_recorder.exportMonoWAV(function (blob) {
        //console.log(' @ stop recording ------------------------------ 3-2 ');
        vf_ReqAudio = blob;
        let url = URL.createObjectURL(blob);
        let audio = document.getElementById("AudioPlayer_req");
        audio.src = url;
        audio.addEventListener('ended', reqAudio_pause_evt);
        set_VF_playUI('req');
    }, "audio/wav", 16000);

    set_VF_afterRecordUI();
    setReadyAudioAPI(null);
}

function onClick_VF_deleteMixedAudio() {
    stopAndClearRecording();

    vf_MixedAudio = null;
    $('#vfMixedInputFile').val(null);
    setReadyAudioAPI(VF_audioErr_callback);

    set_VF_recordUI("mix");
}

function onClick_VF_deleteReqAudio() {
    stopAndClearRecording();

    vf_ReqAudio = null;
    $('#vfReqInputFile').val(null);
    setReadyAudioAPI(VF_audioErr_callback);

    set_VF_recordUI("req");
}

/* ===================================================================================================================== */
// UI 설정
/* ===================================================================================================================== */

function init_VF_Popup() {
    console.log(" @ init Voice Filter ! ");
    $('#voiceFilter_input_box .recording').hide();
    $('#voiceFilter_input_box .upload_box').show();

    $('.MixVoice_play_btn').hide();
    $('.MixVoice_delete').hide();
    $('.ReqVoice_play_btn').hide();
    $('.ReqVoice_delete').hide();

    $('#voiceFilter_input_box .input_box').css('display', 'block');
    $('#voiceFilter_input_box').show();
}

function set_VF_recordUI(classifyParam) {
    console.log(" @ set_VF_recordUI ==> " + classifyParam);

    if (classifyParam == 'mix') {

        $('#mixAudioRecord_btn').show();
        $('#vf_mixAudio .upload_box').show();
        $('#vf_mixAudio .MixVoice_play_btn').hide();
        $('#vf_mixAudio .MixVoice_delete').hide();

    } else if (classifyParam == 'req') {

        $('#reqAudioRecord_btn').show();
        $('#vf_reqAudio .upload_box').show();
        $('#vf_reqAudio .ReqVoice_play_btn').hide();
        $('#vf_reqAudio .ReqVoice_delete').hide();
    }
}

function set_VF_afterRecordUI() {
    $('#voiceFilter_input_box .input_box').show();
    $('#voiceFilter_input_box .btn').show();
    $('#voiceFilter_input_box .recording').hide();
    $('#voiceFilter_input_box .recording .record_Box').hide();
    $('#voiceFilter_input_box .recording .record_desc').hide();
}

function set_VF_playUI(classifyParam) {

    console.log('set_VF_playUI ==> ' + classifyParam);

    if (classifyParam == 'mix') {

        $('#mixAudioRecord_btn').hide();
        $('#vfMixedInputFile').parent().css('display', 'none');

        $('#voiceFilter_input_box .MixVoice_play_btn').show();
        $('#voiceFilter_input_box .MixVoice_delete').show();

    } else if (classifyParam == 'req') {

        $('#reqAudioRecord_btn').hide();
        $('#vfReqInputFile').parent().hide();

        $('#voiceFilter_input_box .ReqVoice_play_btn').show();
        $('#voiceFilter_input_box .ReqVoice_delete').show();
    }

}

/* ===================================================================================================================== */
// Audio 재생
/* ===================================================================================================================== */
function playVFAudio(classifyParam) {
    if (classifyParam == 'mix') {
        $('#AudioPlayer_req')[0].pause();
        let text = $('.ReqVoice_play_btn').text();
        if (text === messages.common.pause)
            $('.ReqVoice_play_btn').text(messages.common.play);

        $('#AudioPlayer_mix')[0].pause();
        $('#AudioPlayer_mix')[0].load();
        $('#AudioPlayer_mix')[0].oncanplaythrough = $('#AudioPlayer_mix')[0].play();
    } else if (classifyParam == 'req') {
        $('#AudioPlayer_mix')[0].pause();
        let text = $('.MixVoice_play_btn').text();
        if (text === messages.common.pause)
            $('.MixVoice_play_btn').text(messages.common.play);

        $('#AudioPlayer_req')[0].pause();
        $('#AudioPlayer_req')[0].load();
        $('#AudioPlayer_req')[0].oncanplaythrough = $('#AudioPlayer_req')[0].play();
    }
}

function pauseVFAudio(classifyParam) {
    if (classifyParam == 'mix')
        $('#AudioPlayer_mix')[0].pause();
    else if (classifyParam == 'req')
        $('#AudioPlayer_req')[0].pause();
}

/* ===================================================================================================================== */
// 공통 function
/* ===================================================================================================================== */
function clear_VF_inputData() {
    vf_MixedAudio = null;
    vf_ReqAudio = null;
    $('#mixAudioRecord_btn').show();
    $('#reqAudioRecord_btn').show();
    $('#vfMixedInputFile').show().val(null);
    $('#vfReqInputFile').show().val(null);
}

function VF_audioErr_callback() {
    $('#voiceFilter_input_box .record_btn').attr('disabled', true);
    $('#voiceFilter_input_box .record_btn').addClass('disabled');
}