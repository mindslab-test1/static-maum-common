/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.ai_styling] = callApi_TTI;

/*
** API 실행
*/
function callApi_TTI(input_data, flow_node, engine) {
    return new Promise ((resolve,reject) => {
        let data = {};
        data.input_type = 'image';
        data.engineName = engine.name;

        console.log('--> [ TTI ] 실행');

        var formData = new FormData;

        formData.append('reqText', input_data);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        mvp_request = new XMLHttpRequest();

        mvp_request.timeout = 20000;
        mvp_request.responseType = "blob";

        mvp_request.open('POST', '/mvp/runner/api/tti');
        mvp_request.send(formData);

        mvp_request.onerror = function() {
            console.log("========> Fail");
            data.errMsg = messages.engineError.output;
            reject(data);
        }

        mvp_request.onreadystatechange = function () {
            if (mvp_request.readyState === 4) {
                console.dir(mvp_request.response);
                if(mvp_request.status === 200) {
                    if (mvp_request.response === 1175) {
                        console.log(engine.name + " ==> 이미지가 존재하지 않습니다. ");
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        //alert('TTI 이미지가 존재하지 않습니다.');
                    }
                    else {
                        data.resp_data = new File([mvp_request.response], "AI_styling.jpg",{type:'image/jpeg'});
                        resolve(data);
                    }
                }
                else {
                    console.log(engine.name + "========> Fail");
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            }
            if (mvp_request.readyState === 0) {
                console.log(engine.name + " Request abort()");
                return false;
            }
        };
    });


    /*
        mvp_request = $.ajax({
            type: 'post',
            data: {
                "reqText": input_data
            },
            url: '/mvp/runner/api/tti',
            timeout: 20000,
            success: function(result) {
                var output_data = null;
                try {
                    console.log("========> OK");
                    console.dir(result);

                    output_data = data;
                } catch (e) {
                    console.log('TTI: 결과 수신 에러');
                }

                controlFlow('image', output_data);
            },
            error: function(error, exception) {
                if(exception === "abort"){
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                controlFlow('image', null);
            }
        });
        */
}