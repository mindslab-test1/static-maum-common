/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.hmd] = callApi_HMD;

/*
** API 실행
*/
function callApi_HMD(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name, input_data.length, 1000)) {
            data.errMsg = messages.mvp_exec_api.error_charLimit_exceed + "1000";
            reject(data);
        }
        else {
            console.log('--> [ HMD ] 실행');

            mvp_request = $.ajax({
                type: 'post',
                data: {
                    "reqText": input_data,
                    "lang": flow_node.paramList[0].value
                },
                url: '/mvp/runner/api/hmd',
                timeout: 20000,
                success: function (result) {
                    var output_data = null;

                    try {
                        var obj = JSON.parse(result);
                        console.log("========> OK");
                        console.dir(result);

                        if ($.isEmptyObject(obj)) {
                            console.log("    # error >> empty response data")
                            data.errMsg = messages.engineError.output;
                            reject(data)
                            return
                        }

                        output_data = result.toString();
                        data.resp_data = output_data;
                        resolve(data);
                    } catch (e) {
                        console.log('HMD: 결과 수신 에러');
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }

                },
                error: function (error, exception) {
                    if (exception === "abort") {
                        console.log(engine.name + " Request abort()");
                        return false;
                    }
                    console.log("========> Fail");
                    console.log(error.toString());

                    data.errMsg = messages.engineError.output;
                    reject(data)
                }
            });
        }
    });
}