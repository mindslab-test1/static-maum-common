/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.text_conv] = callApi_TextConv;

function callApi_TextConv(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        console.log('--> [ 텍스트 변환 ] 실행');

        for (let xx = 0; xx < flow_node.paramList.length; xx++) {
            if (typeof flow_node.paramList[xx].name != 'undefined') {
                mvp_request = $.ajax({
                    type: 'post',
                    data: {
                        "inputData": input_data,
                        "pattern": flow_node.paramList[xx].value
                    },
                    url: '/mvp/runner/api/textConv',
                    timeout: 20000,
                    success: function (result) {
                        if ($.isEmptyObject(result)) {
                            console.log("    # error >> empty response data")
                            data.errMsg = messages.engineError.output;
                            reject(data);
                            return
                        }

                        data.resp_data = result;
                        resolve(data);
                    },
                    error: function (error, exception) {
                        if (exception === "abort") {
                            console.log(engine.name + " Request abort()");
                            return false;
                        }
                        console.log("========> Fail");
                        console.log(error.toString());
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                });
                return;
            }
        }
    })


}