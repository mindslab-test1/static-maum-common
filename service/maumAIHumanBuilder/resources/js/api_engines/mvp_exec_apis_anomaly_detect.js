/*
** 아래 배열에 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.anom_det] = callApi_AnomalyDetect;

/*
** API 실행
*/
function callApi_AnomalyDetect(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'multi';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name, Math.round(input_data.size / 1024), 51200)) { // 50MB = 51200KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data)
        }
        else {
            console.log('--> [ AnomalyDetect ] 실행');
            console.log('    * video : ', input_data);

            let formData = new FormData();
            formData.append('file', input_data);

            mvp_request = $.ajax({
                url : "/mvp/runner/api/anomalyDetect",
                method : "POST",
                async: true,
                data : formData,
                processData: false,
                contentType: false,
                success : function(response){
                    let resp_data = JSON.parse(response);

                    if( $.isEmptyObject(resp_data)){
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }

                    data.resp_data = JSON.parse(response);
                    resolve(data);
                },
                error : function(error, exception){
                    if(exception === "abort"){
                        console.log(engine.name + " Request abort()");
                        return false;
                    }
                    console.log("========> Fail");
                    console.log(error.toString());

                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            });
        }
    });
}