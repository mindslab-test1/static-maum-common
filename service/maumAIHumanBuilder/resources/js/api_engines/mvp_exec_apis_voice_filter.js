/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.voice_filter] = callApi_VoiceFilter;

/*
** API 실행
*/
function callApi_VoiceFilter(input_data, flow_node, engine) { //mixedVoice, reqVoice
    return new Promise((resolve, reject) => {
            let data = {};
            data.input_type = 'audio';
            data.engineName = engine.name;

            console.log("--> [ Voice Filter ] 실행");
            console.log("===> input_data : " + input_data);



            if ((inputSizeValidation(engine.name, Math.round(input_data['mixedVoice'].size / 1024), 2048))
                || (inputSizeValidation(engine.name, Math.round(input_data['reqVoice'] / 1024), 2048))) { // 2MB = 2048KB
                data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
                reject(data);

                $('#mixAudioRecord_btn').show();
                $('#reqAudioRecord_btn').show();
                $('.fr_box .upload_box').show();
            } else {

                let mixFile = new File([input_data.mixedVoice], "1.wav");
                let reqFile = new File([input_data.reqVoice], "2.wav");

                var formData = new FormData();
                formData.append('mixedVoice', mixFile);
                formData.append('reqVoice', reqFile);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                mvp_request = new XMLHttpRequest();

                mvp_request.timeout = 10000;
                mvp_request.responseType = "blob";

                mvp_request.open('POST', '/mvp/runner/api/voiceFilter');
                mvp_request.send(formData);

                mvp_request.onerror = function () {
                    console.log("========> Fail");
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
                mvp_request.onreadystatechange = function () {
                    if (mvp_request.readyState === 4) {
                        console.log(mvp_request.response);

                        if (mvp_request.status === 200) {
                            let audioSrcURL = URL.createObjectURL(mvp_request.response);

                            resetAudio(); // empties any existing information on the audio player
                            newAudioSetup(audioSrcURL); // sets up audio player with new source and plays it

                            $("#AudioPlayer_TTS").off('ended').on('ended', function () {
                                data.resp_data = mvp_request.response;
                                resolve(data);
                            });

                        } else {
                            console.log('request status : ' + mvp_request.status);
                            data.errMsg = messages.engineError.output;
                            reject(data);
                        }

                        $('#mixAudioRecord_btn').show();
                        $('#reqAudioRecord_btn').show();
                        $('.fr_box .upload_box').show();
                    }

                    if (mvp_request.readyState === 0) {
                        console.log(engine.name + " Request abort()");
                        return false;
                    }
                }
                ;
            }
        }
    );
}