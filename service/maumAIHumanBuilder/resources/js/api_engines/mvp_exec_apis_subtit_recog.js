/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.subt_extr] = callApi_SubtitRecog;

/*
** API 실행
*/
function callApi_SubtitRecog(input_data, flow_node, engine) {
    return new Promise( (resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name,  Math.round(input_data.size / 1024),5120)) { // 5MB = 5120KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "5MB";
            reject(data);
        }
        else {
            console.log('--> [ Subtitle Recog ] 실행');

            let formData = new FormData();
            formData.append('file', input_data);
            // formData.append('${_csrf.parameterName}', '${_csrf.token}');

            mvp_request = $.ajax({
                type: 'post',
                data: formData,
                url: '/mvp/runner/api/subtitRecog',
                timeout: 20000,
                processData: false,
                contentType: false,
                success: function (result) {
                    console.log("Subtitle Recog result : ");
                    console.dir(result);

                    if ($.isEmptyObject(result)) {
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return
                    }

                    if (result.result_text === "False"){ // result.data?
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                    else {
                        data.resp_data = result.result_text; // result.data?
                        resolve(data);
                    }
                },
                error: function (error, exception) {
                    if (exception === "abort") {
                        console.log(engine.name + " Request abort()");
                        return false;
                    }
                    console.log("========> Fail");
                    console.dir(error);

                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            });
        }
    });
}