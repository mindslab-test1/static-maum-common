/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.logic_box] = callApi_LogicBox;

/*
** API 실행
*/
function callApi_LogicBox(input_data, flow_node, engine, nodeIndex) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        // 불필요한 line break를 제거해줌
        input_data = input_data.replace(/(\r\n|\n|\r)/gm,"")

        var param_list = new Array();

        getParamListFromFlowNode(flow_node, param_list);
        console.dir(param_list);

        // 사용자 로직박스 설정을 수집함
        let reqInfo = getLogicBoxRequestInfo(input_data, nodeIndex)
        let respInfo = getLogicBoxResponseInfo(nodeIndex)

        let reqData = JSON.stringify({
            "endpoint": param_list[1].value,
            "reqMethod": param_list[0].value,
            "contentType": reqInfo.contentType,
            "reqParamList": reqInfo.reqParams,
            "reqHeaderList": reqInfo.reqHeaders,
            "reqBody": reqInfo.reqBody,
            "jsonPathList": respInfo.jsonPaths
        })

        console.log('--> [ Logic Box ] 실행');
        console.log('    * input : ', input_data);
        console.log('    * reqData : ', reqInfo);
        console.log('    * respData : ', respInfo);
        console.log(reqData)

        mvp_request = $.ajax({
            type: "post",
            dataType: "text",
            contentType: "application/json",
            data: reqData,
            url: "/mvp/runner/api/logicBox",
            timeout: 20000,
            success: function (result) {
                var output_data = null;

                try {
                    console.log("========> OK");
                    console.dir(result);
                    if ($.isEmptyObject(result)) {
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data)
                        return
                    }
                    else if (result.includes("_jsonMapError_")) { // 매핑된 output이 텍스트나 숫자가 아닌경우
                        console.log("    # error >> response data is either JSON or array")
                        data.errResponse = result.replace("_jsonMapError_","");
                        data.jsonPath = respInfo.jsonPaths[0].jsonPath;
                        data.errMsg = messages.logicBox.jsonMapError;
                        reject(data)
                        return
                    }
                    else if (result.includes("_apiRequestError_")) { // 외부 API에서 오류가 발생했을 경우
                        console.log("    # error >> API Request error")
                        data.errResponse = result.replace("_apiRequestError_","");
                        data.errMsg = messages.logicBox.api;
                        reject(data)
                        return
                    }
                    else if (result.includes("_jsonProcessingError_")) { // 외부 API에서 오류가 발생했을 경우
                        console.log("    # error >> JSON processing error due to incorrect mapping param")
                        data.errResponse = result.replace("_jsonProcessingError_","");
                        data.jsonPath = respInfo.jsonPaths[0].jsonPath;
                        data.errMsg = messages.logicBox.jsonProcessingError;
                        reject(data)
                        return
                    }

                    // /*외부 API에서 오류시*/
                    // if (result._error_) {
                    //
                    // }

                    output_data = result;
                    data.resp_data = output_data;
                    resolve(data);
                } catch (e) {
                    console.log('Logic Box: 결과 수신 에러');
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            },
            error: function (error, exception) {
                if (exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data)
            }
        })
    });
}

// Request popup form에 적힌 API 설정을 object로 패키징
function getLogicBoxRequestInfo(input_data, nodeIndex) {
    let divId = '#pop_request_add' + nodeIndex, // Node index에 따라 팝업 div의 id에 숫자를 매김
        requestContType = $(divId + ' .request_contType_value').val(),
        requestHeaders = [];

    $(divId + ' .request_headers_table tr.request_headers').each(function () { // 헤더 정보 패키징
        let checkBox = $(this).find('input[type="checkbox"]').prop("checked"),
            key = $(this).find('input.key').val(),
            value = $(this).find('input.value').val(),
            desc = $(this).find('input.desc').val();
        if (checkBox && key && value) {
            requestHeaders.push({
                key: key,
                val: value,
                desc: desc
            });
        }
    });

    let requestParams = [];
    let requestBody = "";
    if (requestContType === 'application/x-www-form-urlencoded') { // Body는 보지않고 Parameter 정보만 가져옴
        $(divId + ' .request_params_table tr.request_params').each(function () { // 파라미터 정보 패키징
            let checkBox = $(this).find('input[type="checkbox"]').prop("checked"),
                key = $(this).find('input.key').val(),
                desc = $(this).find('input.desc').val(),
                value = $(this).find('input.value').val();

            if (value === "${data}") { // 사용자의 value가 ${data}일 때, 사용자의 input_data를 value로 쓴다.
                value = input_data;
            }

            if (checkBox && key && value) {
                requestParams.push({
                    key: key,
                    val: value,
                    desc: desc
                });
            }
        });
    }
    else { // Content-type: application/json일때, Parameter는 보지않고 JSON Body만 본다
        requestBody = $(divId + ' .request_body_content').val();
        // 사용자의 value가 ${data}일 때, 사용자의 input_data를 value로 쓴다.
        requestBody = requestBody.replaceAll('${data}', '\"' + input_data + '\"');
        requestBody = JSON.parse(requestBody)
        requestBody = JSON.stringify(requestBody)
    }

    let requestObj = {
        contentType: requestContType,
        reqHeaders: requestHeaders,
        reqParams: requestParams,
        reqBody: requestBody
    };

    console.log(requestObj);
    return requestObj;
}

// Response popup form에 적힌 API 설정을 object로 패키징
function getLogicBoxResponseInfo(nodeIndex) {
    let divId = '#pop_response_add' + nodeIndex, // node index에 따라 div의 id에 숫자를 매김
        responseOutputs = [];

    $(divId + ' .response_output_table tr.response_outputs').each(function () { // output 정보 패키징
        let checkBox = $(this).find('input[type="checkbox"]').prop("checked"),
            key = $(this).find('input.key').val(),
            value = $(this).find('input.value').val();
        if (checkBox && key && value) {
            responseOutputs.push({
                jsonPath: key,
                mappingParam: value
            });
        }
    });
    let responseObj = {
        jsonPaths: responseOutputs
    };
    console.log(responseObj);
    return responseObj;
}