/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.denoise] = callApi_Denoise;

/*
** API 실행
*/
function callApi_Denoise(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'audio';
        data.engineName = engine.name;
        console.log('--> [ Denoise ] 실행');

        if (inputSizeValidation(engine.name, Math.round(input_data.size / 1024), 2048)) { // 2MB = 2048KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data)
        }
        else {
            var formData = new FormData();
            formData.append('noiseFile', input_data);
            //formData.append('${_csrf.parameterName}', '${_csrf.token}');

            mvp_request = new XMLHttpRequest();

            mvp_request.timeout = 10000;
            mvp_request.responseType = "blob";

            mvp_request.open('POST', '/mvp/runner/api/denoise');
            mvp_request.send(formData);

            mvp_request.onerror = function () {
                data.errMsg = messages.engineError.output;
                reject(data)
            }

            mvp_request.onreadystatechange = function () {
                if (mvp_request.readyState === 4) {
                    console.log(mvp_request.response);

                    if (mvp_request.status === 200) {
                        let audioSrcURL = URL.createObjectURL(mvp_request.response);

                        if ($.isEmptyObject(mvp_request.response)) {
                            console.log("    # error >> empty response data")
                            data.errMsg = messages.engineError.output;
                            reject(data);
                            return
                        }

                        resetAudio(); // empties any existing information on the audio player
                        newAudioSetup(audioSrcURL); // sets up audio player with new source and plays it
                        // TODO: fix audio playing once the popup is closed

                        $("#AudioPlayer_TTS").off('ended').on('ended', function () {
                            data.resp_data = mvp_request.response;
                            resolve(data)
                        });
                    } else {
                        console.error("Response code : " + mvp_request.status + "\nResponse message : " + mvp_request.statusText);
                        data.errMsg = messages.engineError.output;
                        reject(data)
                    }
                }
                if (mvp_request.readyState === 0) {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };
        }
    })

}