/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.face_rec_view] = callApi_getFace;
api_func_link[messages.engine.face_rec_set] = callApi_setFace;
api_func_link[messages.engine.face_rec_del] = callApi_delFace;
api_func_link[messages.engine.face_rec_rec] = callApi_recogFace;

/*
** API 실행
*/
function callApi_getFace(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        console.log('--> [ Face Recog : getFace ] 실행');
        console.log("===> input_data : " + input_data);
        var param_list = new Array();
        getParamListFromFlowNode(flow_node, param_list);
        console.dir(param_list);

        var formData = new FormData();

        // dbId 세팅
        formData.append('dbId','default');
        // for (var xx = 0; xx < param_list.length; xx++) {
        //     formData.append(param_list[xx].name, param_list[xx].value);
        //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
        // }

        mvp_request = new XMLHttpRequest();
        mvp_request.responseType = "json";

        mvp_request.open('POST', '/mvp/runner/api/faceRecog/get');
        mvp_request.send(formData);

        mvp_request.onerror = function () {
            data.errMsg = messages.engineError.output;
            reject(data);
        }

        mvp_request.onreadystatechange = function () {
            if (mvp_request.readyState === 4) {
                console.dir(mvp_request.response);

                if (mvp_request.status === 200) {
                    console.log("    # result >> ", mvp_request.response);
                    if ($.isEmptyObject(mvp_request.response)) {
                        console.log("    # error >> empty response data")

                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return
                    }
                    data.resp_data = mvp_request.response;
                    resolve(data);
                } else {
                    console.log("request status : " + mvp_request.status);
                    data.errMsg = messages.engineError.output;
                    reject(data)
                    return
                }
            }

            if (mvp_request.readyState === 0) {
                console.log(engine.name + " Request abort()");
                return false;
            }
        };
    })
}

function callApi_setFace(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name, Math.round(input_data.image.size / 1024), 3072)) { // 3MB = 3072KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "3MB";
            reject(data);
        }
        else {
            console.log('--> [ Face Recog : setFace ] 실행');
            console.log("===> input_data : " + input_data);

            var param_list = new Array();
            getParamListFromFlowNode(flow_node, param_list);
            console.dir(param_list);

            let formData = new FormData();
            let img_file = new File([input_data.image], 'faceRecog_file.jpg');

            // for (let xx = 0; xx < param_list.length; xx++) {
            //     formData.append(param_list[xx].name, param_list[xx].value);
            //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
            // }
            formData.append('dbId', 'default');
            formData.append('file', img_file);
            formData.append('faceId', input_data.faceId);

            mvp_request = new XMLHttpRequest();
            mvp_request.responseType = "json";

            mvp_request.open('POST', '/mvp/runner/api/faceRecog/set');
            mvp_request.send(formData);

            mvp_request.onerror = function () {
                data.errMsg = messages.engineError.output;
                reject(data)
            }

            mvp_request.onreadystatechange = function () {
                if (mvp_request.readyState === 4) {
                    console.dir(mvp_request.response);
                    if (mvp_request.status === 200) {
                        console.log("    # result >> ", mvp_request.response);

                        if ($.isEmptyObject(mvp_request.response)) {
                            console.log("    # error >> empty response data")
                            data.errMsg = messages.engineError.output;
                            reject(data);
                            return;
                        }
                        data.resp_data = mvp_request.response;
                        resolve(data);
                    } else {
                        console.error("Response code : " + mvp_request.status + "\nResponse message : " + mvp_request.statusText);
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return;
                    }
                }

                if (mvp_request.readyState === 0) {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };
        }
    });
}

function callApi_delFace(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        console.log('--> [ Face Recog : delFace ] 실행');
        console.log("===> input_data : " + input_data);

        var param_list = new Array();
        getParamListFromFlowNode(flow_node, param_list);
        console.dir(param_list);

        let formData = new FormData();

        // for (let xx = 0; xx < param_list.length; xx++) {
        //     formData.append(param_list[xx].name, param_list[xx].value);
        //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
        // }
        formData.append('dbId', 'default');
        formData.append('faceId', input_data);

        mvp_request = new XMLHttpRequest();
        mvp_request.responseType = "json";

        mvp_request.open('POST', '/mvp/runner/api/faceRecog/delete');
        mvp_request.send(formData);

        mvp_request.onerror = function () {
            data.errMsg = messages.engineError.output;
            reject(data);
        }

        mvp_request.onreadystatechange = function () {
            if (mvp_request.readyState === 4) {
                console.dir(mvp_request.response);

                if (mvp_request.status === 200) {
                    console.log("    # result >> ", mvp_request.response);
                    if ($.isEmptyObject(mvp_request.response)) {
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return
                    }
                    data.resp_data = mvp_request.response;
                    resolve(data)
                } else {
                    data.errMsg = messages.engineError.output;
                    reject(data);
                    return
                }
            }

            if (mvp_request.readyState === 0) {
                console.log(engine.name + " Request abort()");
                return false;
            }
        };
    });
}


function callApi_recogFace(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name, Math.round(input_data.size / 1024), 3072)) { // 3MB = 3072KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "3MB";
            reject(data);
        }
        else {
            console.log('--> [ Face Recog : recogFace ] 실행');
            console.log("===> input_data : " + input_data);

            var param_list = new Array();
            getParamListFromFlowNode(flow_node, param_list);
            console.dir(param_list);

            let formData = new FormData();
            let img_file = new File([input_data], 'faceRecog_file.jpg');
            formData.append('file', img_file);
            formData.append('dbId', 'default');

            // for (let xx = 0; xx < param_list.length; xx++) {
            //     formData.append(param_list[xx].name, param_list[xx].value);
            //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
            // }

            mvp_request = new XMLHttpRequest();
            mvp_request.responseType = "json";

            mvp_request.open('POST', '/mvp/runner/api/faceRecog/recog');
            mvp_request.send(formData);

            mvp_request.onerror = function () {
                data.errMsg = messages.engineError.output;
                reject(data);
            }

            mvp_request.onreadystatechange = function () {
                if (mvp_request.readyState === 4) {
                    console.dir(mvp_request.response);

                    if (mvp_request.status === 200) {
                        console.log("    # result >> ", mvp_request.response);
                        if ($.isEmptyObject(mvp_request.response)) {
                            data.errMsg = messages.engineError.output;
                            reject(data)
                            return;
                        }
                        data.resp_data = mvp_request.response.result.id
                        resolve(data)
                    } else {

                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                }

                if (mvp_request.readyState === 0) {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };
        }
    });
}