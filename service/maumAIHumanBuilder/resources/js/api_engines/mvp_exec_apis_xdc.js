/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.xdc] = callApi_xdc;


/*
** API 실행
*/
function callApi_xdc(input_data, flow_node, engine) {
    return new Promise ((resolve,reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        console.log('--> [ XDC ] 실행');

        mvp_request = $.ajax({
            type: 'post',
            data: {
                "context": input_data
            },
            url: '/mvp/runner/api/xdc',
            timeout: 20000,
            success: function (result) {

                try {
                    let resp_data = JSON.parse(result);
                    console.log("========> OK");
                    console.dir(result);
                    if (resp_data['message']['status'] === 5000) {
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                    else {
                        data.resp_data = result;
                        resolve(data);
                    }
                } catch (e) {
                    console.log('XDC: 결과 수신 에러');
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            },
            error: function (error, exception) {
                if (exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data);
            }
        });
    });
}