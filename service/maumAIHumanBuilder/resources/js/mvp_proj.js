let newProject = new Flow('', '');
let reordered = false;
let projectNameChanged = [];
let currentProjectIndex = -1;
let currentProjects = [];
let deleted = false;
let projectListCache
let sortableProjectListCache
let currentProjectsCache

$(document).ready(function () {
    // 사용자 저장된 프로젝트 불러오기
    getProjects(user_no);

    // New Project Pop-up
    $('.btn_project_add').on('click', function (e) {
        e.preventDefault()

        $('.lyr_bg').one('click', onCancelAddNewProject);

        $(this).delay(300).queue(function () {
            $('#pop_pjt_add').find('.ipt_txt').focus();
        });
    });

    // Add new project
    $('#btn_pjt_add').on('click', onAddNewProject);

    //프로젝트 등록
    $('#pop_pjt_add').each(function () {
        //등록버튼 활성화
        $('#pop_pjt_add .ipt_txt, #pop_pjt_add .textArea').on('input', function (event) {
            //등록버튼 Validation
            checkInputLength();
            isNewProjectNameDuplicate();
        });
    });

    $('.btn_pjt_setup').on('click', function (e) {
        e.preventDefault()
        setUpProjectManagement();
        $('#pop_pjt_modify #btn_pjt_modify').on('datachanged', function () {
            console.log($(this).data())
            let validUpdateFlags = $(this).data();

            if (validUpdateFlags.reordered || validUpdateFlags.nameUpdated || validUpdateFlags.deleted) {
                if (!validUpdateFlags.isDuplicate && !validUpdateFlags.duplicateInput) {
                    $('#pop_pjt_modify #btn_pjt_modify').prop("disabled", false);
                    $('#pop_pjt_modify .lyr_mid .error').removeClass("active");

                } else {
                    $('#pop_pjt_modify .lyr_mid .error').addClass("active");
                    $('#pop_pjt_modify #btn_pjt_modify').prop("disabled", true);

                }
            } else {
                $('#pop_pjt_modify #btn_pjt_modify').prop("disabled", true);

            }
        })
        $('.lyr_bg').one('click', onCancelUpdateProject);
    })

    //프로젝트 관리 저장 클릭시
    $('#pop_pjt_modify #btn_pjt_modify').on('click', onSaveUpdatedProject)


    // On cancel button clicked
    $('.btn_lyr_close.btn_lyr_cancel').on('click', function (e) {
        //Project modify or project add canceled
        if ($(e.target).parents('#pop_pjt_modify').length > 0) {
            onCancelUpdateProject();
        } else if ($(e.target).parents('#pop_pjt_add').length > 0) {
            onCancelAddNewProject()
        }
    })

})

// Confirm user to save changes before page unload
window.addEventListener('beforeunload', function (e) {
    if (workingFlowStatus.flowUpdated) {
        e.preventDefault();
        e.returnValue = '';
    }
});

/* ===================================================================================================================== */
// 프로젝트 등록
/* ===================================================================================================================== */

//사용자의 새로운 프로젝트 저장
function saveNewProject() {
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(newProject),
        url: '/mvp/api/Flows',
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            console.log("새로운 project 저장 ========> OK");
            console.dir(result);
            // 현재 사용자 프로젝트 추가
            currentProjects.push(result);

            //프로젝트 리스트에 추가
            let newProjElem = $('<li class="' + result.flowId + '"><a href="#none"><span>' + result.name + '</span></a></li>')
            $('.projectBox .dlBox dd ul.lst').append(newProjElem);

            //프로젝트 관리 리스트에 추가
            $('.lyr_mid #sortable').append('<li id="' + result.flowId + '" class="ui-state-default mod-projects"><input placeholder="' + result.name + '">' +
                '<button type="button" class="btn_pjt_delete">' + messages.mvp_proj.delete + '</button></li>')
            $('.projectBox .dlBox dd ul.lst li.dataNone').remove();


            let lastIndex = currentProjects.length - 1
            currentProjectIndex = lastIndex;
            $('.projectBox .dlBox dd ul.lst #' + currentProjects[lastIndex].flowId + ' a')

            onSelectProject(currentProjects[lastIndex]);

            //프로젝트 삭제 클릭시
            $('#sortable .btn_pjt_delete').off('click').on('click', onDeleteProject);

            isUpdatedProjectNameDuplicate();

            console.log("House engine group " + $('.cont_box').find('[class*="House_EngineGroup_"]'))

            initEventListener();
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
            // error popup
        }
    });
}

//프로젝트 등록 취소 클릭시
function onCancelAddNewProject() {
    $('#pop_pjt_add .lyr_mid .dlBox dl.dl_tbl dd .error.active').removeClass('active');
    $('#pop_pjt_add .ipt_txt').val('');
    $('#pop_pjt_add .textArea').val('');
    $('#btn_pjt_add').prop('disabled', true); //버튼 비활성화
}

// 프로젝트 등록 팦엎
function onAddNewProject() {
    console.log("Project add button clicked")
    $("#addNewProject").hide();
    resetEngineStatus(null);

    newProject.name = $('#proj_name').val();
    newProject.description = $('#proj_desc').val();
    newProject.displayOrder = $('.projectBox .dlBox dd ul.lst li').prevAll().length + 1;

    saveNewProject();

    $('#pop_pjt_add', '.btn_lyr_close').trigger('click');
    // onCancelAddNewProject()
}


/* ===================================================================================================================== */
// 프로젝트 조회
/* ===================================================================================================================== */

//사용자 저장된 프로젝트 불러오기
function getProjects(user_no) {
    let sortProjectByDisplayOrder = (project) => {
        let length = project.length;
        for (let i = 1; i < length; i++) {
            let key = project[i];
            let keyOrder = project[i].displayOrder;
            let j = i - 1;
            while (j >= 0 && project[j].displayOrder > keyOrder) {
                project[j + 1] = project[j];
                j = j - 1;
            }
            project[j + 1] = key;
        }
        return project;
    };

    $.ajax({
        type: 'get',
        contentType: 'application/json',
        dataType: 'json',
        url: '/mvp/api/Flows/User/' + user_no,
        timeout: TIMEOUT___MVP_API + 2000,
        success: function (result) {
            console.log("project 불러오기 ========> OK");
            console.dir(result);


            //프로젝트 목록에 추가
            if (result.length > 0) {
                sortProjectByDisplayOrder(result)
                for (let i = 0; i < result.length; i++) {
                    currentProjects.push(result[i]);

                    //프로젝트 목록/관리에 추가
                    appendProjectsToList(result[i]);
                }

                $('.projectBox .dlBox dd ul.lst li.dataNone').remove();
                //프로젝트 삭제 클릭시
                $('#sortable .btn_pjt_delete').off('click').on('click', onDeleteProject);
            } else {
                //프로젝트 추가 안내 메세지
                $('.projectBox .dlBox dd ul.lst').each(function () {
                    var projectLength = $(this).children('li').length;

                    // 프로젝트 없을 경우
                    if (projectLength < 1) {
                        $(this).append('<li class="dataNone">' + messages.mvp_proj.noProjectFound_error + '</li>');
                    } else {
                        $(this).find('li.dateNone').remove();
                    }
                });
            }
            initEventListener();
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        }
    });
}

/* ===================================================================================================================== */
// 프로젝트 삭제
/* ===================================================================================================================== */

//프로젝트 지우기
function deleteProjects(flowIdArr) {
    $.ajax({
        type: 'delete',
        contentType: 'application/json',
        data: JSON.stringify(flowIdArr),
        url: '/mvp/api/Flows/delete',
        timeout: TIMEOUT___MVP_API + 2000,
        success: function (result) {
            console.log("project 지우기 ========> OK");
            $('#sortable .hidden').remove();
            $('.projectBox .dlBox dd ul.lst li.hidden').remove();
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        }
    });
}

/* ===================================================================================================================== */
// 프로젝트 수정
/* ===================================================================================================================== */

//프로젝트 순위 설정
function updateProjectOrder(data) {
    $.ajax({
        type: 'put',
        contentType: 'application/json',
        data: JSON.stringify(data),
        url: '/mvp/api/Flows/displayOrder',
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            console.log("project order update ========> OK");
            console.log(result)
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        }
    });
}

//프로젝트명 설정
function updateProjectName(data) {
    $.ajax({
        type: 'put',
        contentType: 'application/json',
        data: JSON.stringify(data),
        url: '/mvp/api/Flows/name',
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            console.log("project order update ========> OK");
            console.log(result)
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        }
    });
}

/* ===================================================================================================================== */
// 프로젝트 설정
/* ===================================================================================================================== */

function setUpProjectManagement() {
    currentProjectsCache = Array.from(currentProjects);

    projectListCache = $('.projectBox .dlBox dd ul.lst').html().replace(/<\!--.*?-->/g, "");
    sortableProjectListCache = $("#sortable").html().replace(/<\!--.*?-->/g, "");

    onSortable();
    $('#sortable .btn_pjt_delete').off('click').on('click', onDeleteProject);


    let duplicateSet = new Set();
    isUpdatedProjectNameDuplicate(duplicateSet);
}

//프로젝트 등록 리스트에 추가시
function appendProjectsToList(project) {
    $('.projectBox .dlBox dd ul.lst').append('<li class="' + project.flowId + '"><a href="#"><span>' + project.name + '</span></a></li>');
    $('.lyr_mid #sortable').append('<li id="' + project.flowId + '" class="ui-state-default mod-projects"><input placeholder="' + project.name + '">' +
        '<button type="button" class="btn_pjt_delete">' + messages.mvp_proj.delete + '</button></li>')

}

function initEventListener() {
    // 프로젝트 클릭시
    $('.projectBox .dlBox dd ul.lst li a').off('click').on('click', function (e) {
        e.preventDefault();
        var index = $(this).parent().prevAll().length;

        // Check for update in current flow
        if (currentProjectIndex == -1 || !workingFlowStatus.flowUpdated) {
            currentProjectIndex = index;
            onSelectProject(currentProjects[index], this);
        } else {
            showPopup_SaveFlow(messages.mvp_proj.save_flow, "selPrj", this, index)
        }
    });
}

/* ===================================================================================================================== */
// EVENT HANDLERS
/* ===================================================================================================================== */


function onSaveUpdatedProject() {
    // projects deleted
    if (deleted) {
        var flow_id = {};
        var arr = [];
        $('#sortable .ui-state-default.hidden').each(function () {
            var checkedFlowId = $(this).attr('id');
            var flowIdNum = parseInt(checkedFlowId);
            arr.push(flowIdNum);

            $('.projectBox .dlBox dd ul.lst .' + checkedFlowId).addClass('hidden');
            $('.projectBox .dlBox dd ul.lst .' + checkedFlowId).hide();
        })
        flow_id.flow_id = arr;
        currentProjects = currentProjects.filter(prj => prj.delYn != "Y")
        deleteProjects(flow_id);
        deleted = false;
    }

    //projects reordered
    if (reordered) {
        let data = []
        currentProjects.forEach((currentProject, index) => {
            let projectOrderings = {}
            projectOrderings.flow_id = currentProject.flowId;
            projectOrderings.displayOrder = index;
            console.log(projectOrderings)
            data.push(projectOrderings);
        })
        console.log(data)
        updateProjectOrder(data);
    }

    currentProjects = currentProjects;

    //projects name updated
    if (projectNameChanged.includes(true)) {
        let data = []
        $('.lyr_mid #sortable li input').each(function (index, element) {
            let changedProjectName = $(this).val();
            if (changedProjectName !== '') {
                $(this).attr('placeholder', changedProjectName)
                currentProjects[index].name = changedProjectName
                $('.projectBox .dlBox dd ul.lst li').eq(index).find('span').text(changedProjectName)

                let project = {}
                project.flowId = currentProjects[index].flowId;
                project.name = currentProjects[index].name;
                data.push(project)
            }
        })
        updateProjectName(data);
    }

    $('#pop_pjt_modify #btn_pjt_modify').prop("disabled", true);
}

//프로젝트 삭제시
function onDeleteProject() {
    currentProjects[$(this).parent().prevAll().length].delYn = "Y"
    $(this).parent().hide();
    $(this).parent().addClass('hidden');
    $('#pop_pjt_modify #btn_pjt_modify').data("deleted", true).trigger('datachanged');

    deleted = true;

    checktProjectNameInputDuplication();
}


//프로젝트 목록 리스트 클릭시
function onSelectProject(project, obj) {
    $("#addNewProject").hide();

    //Disable delete for current project
    let id = currentProjects[currentProjectIndex].flowId;
    $('#sortable li .btn_pjt_delete').prop('disabled', false)
    $('#sortable li .btn_pjt_delete').css({cursor: 'pointer'})
    $('#sortable').find('#' + id + ' .btn_pjt_delete').prop('disabled', true);
    $('#sortable').find('#' + id + ' .btn_pjt_delete').css({cursor: 'default'})


    //Set current project active
    $('.projectBox .dlBox dd ul.lst li a').removeClass('active');
    $(obj).addClass('active');

    // 프로젝트 타이틀 show
    $('.pjt_infoBox').addClass('active');
    var height = $(window).height();
    $('.mvp_contents').css('height', height - 328 + 'px');
    callFlow(project.flowId);

}

//프로젝트 순위 설정
function onSortable() {
    let lst, pre, post; //lst == name of list being sorted
    $("#sortable").sortable({
        start: function (event, ui) {
            pre = ui.item.index();
        },
        stop: function (event, ui) {
            lst = $(this).attr('class');
            post = ui.item.index();
            other = (lst.includes('menu-projects')) ? 'mod-projects' : 'menu-projects';

            // Reorder current projects
            var slicedProj = currentProjects.splice(pre, 1);
            currentProjects.splice(post, 0, slicedProj[0])

            if (post > pre) {
                $('.' + other + ' li:eq(' + pre + ')').insertAfter('.' + other + ' li:eq(' + post + ')');
            } else {
                $('.' + other + ' li:eq(' + pre + ')').insertBefore('.' + other + ' li:eq(' + post + ')');
            }
        }, update: function (event, ui) {
            $('#pop_pjt_modify #btn_pjt_modify').data("reordered", true).trigger('datachanged');
            reordered = true
        }
    });
}

//프로젝트 관리 취소 버튼 클릭시
function onCancelUpdateProject() {
    currentProjects = currentProjectsCache
    deleted = false;

    $('#pop_pjt_modify #btn_pjt_modify').prop("disabled", true);

    // $('#pop_pjt_modify #btn_pjt_modify').data("deleted", true);

    $('#sortable').sortable("cancel");
    $('#sortable .hidden').show();
    $('#sortable .hidden').removeClass('hidden');

    currentProjects.forEach(prj => prj.delYn = "N")

    $('.projectBox .dlBox dd ul.lst li').detach()
    $('.projectBox .dlBox dd ul.lst').append(projectListCache)
    $('#sortable li').detach();
    $('#sortable').append(sortableProjectListCache);
    initEventListener();
}


/* ===================================================================================================================== */
// VALIDATION
/* ===================================================================================================================== */

//프로젝트 등록명 Length Validation
function checkInputLength() {
    var pjt_addIptTxtlength = $('#pop_pjt_add .ipt_txt').val().length,
        pjt_addtextAreaTxtlength = $('#pop_pjt_add .textArea').val().length;

    if (pjt_addIptTxtlength > 1 && pjt_addtextAreaTxtlength > 1) {
        $('#btn_pjt_add').prop('disabled', false); //버튼 활성화
    } else {
        $('#btn_pjt_add').prop('disabled', true); //버튼 비활성화
    }
}

//프로젝트 등록명 Duplicate Validation
function isNewProjectNameDuplicate() {
    let projectName = $('#pop_pjt_add .ipt_txt').val();
    let isDuplicateProjectName = currentProjects.some(currentProject => {
        return currentProject.name === projectName
    })
    if (isDuplicateProjectName) {
        $('.lyrWrap .lyrBox .lyr_mid .dlBox dl.dl_tbl dd .error').addClass('active');
        $('#btn_pjt_add').prop('disabled', true); //버튼 비활성화
    } else {
        $('.lyrWrap .lyrBox .lyr_mid .dlBox dl.dl_tbl dd .error').removeClass('active');
    }
}


function checktProjectNameInputDuplication() {
    let inputValues = []
    $('.lyr_mid #sortable li input').each(function (i) {
        console.log($(this).parent().hasClass('hidden'))
        if (!$(this).parent().hasClass("hidden"))
            if ($(this).val() !== "" && $(this).val().length > 1) inputValues.push($(this).val());
    })

    if ((new Set(inputValues)).size !== inputValues.length) {
        $('#pop_pjt_modify #btn_pjt_modify').data('duplicateInput', true).trigger('datachanged');
    } else {
        $('#pop_pjt_modify #btn_pjt_modify').data('duplicateInput', false).trigger('datachanged');

    }
}

//프로젝트 관리명 Duplicate Validation
function isUpdatedProjectNameDuplicate(duplicateSet) {
    $('.lyr_mid #sortable').off('input', 'li input')
    $('.lyr_mid #sortable').on('input', 'li input', function () {

        let inputProjectName = $(this).val()
        let index = $(this).parent().prevAll().length;
        let inputProjectId = $(this).parent().attr('id');

        // Check if new name is duplicate of current project name
        let isDuplicateProjectName = currentProjects.some(currentProject => {
            return currentProject.name === inputProjectName
        })


        if (isDuplicateProjectName) {
            $('#pop_pjt_modify #btn_pjt_modify').data('isDuplicate', true).trigger('datachanged')
        } else {
            $('#pop_pjt_modify #btn_pjt_modify').data('isDuplicate', false).trigger('datachanged');
        }

        checktProjectNameInputDuplication();


        // Check if the updated project name is more than 2 characters long
        if (!isDuplicateProjectName) {
            projectNameChanged = [];
            $('.lyr_mid #sortable li input').each(function () {
                if ($(this).val().length > 1) {
                    projectNameChanged.push(true);
                } else {
                    projectNameChanged.push(false);
                }
            })
            $('#pop_pjt_modify #btn_pjt_modify').data('nameUpdated', projectNameChanged.includes(true)).trigger('datachanged')
        }
    })
}
