let mvpDataType = {
    TEXT    : { value:1, name:"text" },
    IMAGE   : { value:2, name:"image" },
    AUDIO   : { value:3, name:"audio" },
    VIDEO   : { value:4, name:"video" },
    JSON    : { value:5, name:"json" }
}