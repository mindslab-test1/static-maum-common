/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.chatbot] = callApi_Chat;

var chat_auth_token;
var chat_model;
var chat_question;
var chat_device_id = '';

function callApi_Chat(input_data, flow_node, engine, handler) {
    console.log('--> [ 챗봇 ] 실행');
    console.log('    * model : ', flow_node.paramList[0].value);
    console.log('    * question : ', input_data);

    chat_model = flow_node.paramList[0].value;
    chat_question = input_data;
    chatSingIn(handler); // -> chatOpen(chatbot); -> sendTalk(talkMessage);
}

function chatSingIn(handler) {
    $.ajax({
        url: 'https://aicc-prd1.maum.ai:9980/api/v3/auth/signIn',
        async: false,
        type: 'POST',
        timeout: 20000,
        headers: {
            "Content-Type": "application/json",
            "m2u-auth-internal": "m2u-auth-internal"
        },
        data: JSON.stringify({
            "userKey": "admin",
            "passphrase": "1234"
        }),
        dataType: 'json',
        success: function(data) {
            chat_auth_token = data.directive.payload.authSuccess.authToken;
            console.log('chatbot login success!');
            chatOpen(chat_model, handler);
        }, error: function(err) {
            console.log("chatbot Login error! ", err);
            handler.controlFlow(null);
        }
    });
}

function chatOpen(chatbot, handler) {
    chat_device_id = 'MAUM_' + this.randomString();
    $.ajax({
        url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/open',
        async: false,
        type: 'POST',
        timeout: 20000,
        headers: {
            "Content-Type": "application/json",
            "m2u-auth-internal": "m2u-auth-internal"
        },
        data: JSON.stringify({
            "payload": {
                "utter": "UTTER1",
                "chatbot": chatbot,
                "skill": "SKILL1"
            },
            "device": {
                "id": chat_device_id,
                "type": "WEB",
                "version": "0.1",
                "channel": "ADMINWEB"
            },
            "location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
            "authToken": chat_auth_token
        }),
        dataType: 'json',
        success: function () {
            console.log('chatbot open success!');
            console.dir(chat_question);
            sendTalk(chat_question, handler);
        }, error: function (err) {
            console.log("chatbot open error! ", err);
            handler.controlFlow(null);
        }
    });
}

function sendTalk(Message, handler) {
    Message = Message.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, " ");
    $.ajax({
        url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/textToTextTalk',
        async: true,
        type: 'POST',
        timeout: 10000,
        headers: {
            "Content-Type": "application/json",
            "m2u-auth-internal": "m2u-auth-internal"
            //"Authorization": document.getElementById('AUTH_ID').value
        },
        data: JSON.stringify({
            "payload": {"utter": Message, "lang": "ko_KR"},
            "device": {
                "id": chat_device_id,
                "type": "WEB",
                "version": "0.1",
                "channel": "ADMINWEB"
            },
            "location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
            "authToken": chat_auth_token
        }),
        dataType: 'json',
        success: function(data) {
            console.dir(data);
            if(data.directive !== undefined){
                let result = data.directive.payload.response.speech.utter;
                handler.controlFlow('text', result);
            }else{
                console.log("chatbot answer error");
                console.dir(data);
                handler.controlFlow('text', null);
            }

        }, error: function(err) {
            console.log("chatbot SendTalk error! ", err);
            handler.controlFlow('text', null);
        }
    });
}

function  randomString() {
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    const string_length = 15;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
        let rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}

