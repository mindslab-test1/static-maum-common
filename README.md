# 공통 리소스 배포를 위한 프로젝트

## 소스 디렉토리

/common 공통 리소스 최상위 디렉토리     
      
## 소스 -> URL 매핑 
| 소스 | URL |
|---|:---|
|/common | https://dev-static.maum.ai/common/ (DEV 환경) |
|/common | https://stg-static.maum.ai/common/ (STG 환경) |
|/common | https://static.maum.ai/common/ (PROD 환경) |

*ex) /common/css/all.css*  --> https://stg-static.maum.ai/common/resources/css/main.css


## 배포 방법
jenkins: http://10.122.64.57:9090/ 접속하여 static 프로젝트 build.

